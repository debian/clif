/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/* 
 * input.h
 *
 * Redefinition of the input function for yacc.
 */

#ifndef _INPUT_H
#define _INPUT_H

#ifdef input
#undef input
#endif

#ifndef FLEX_SCANNER
extern int (*input) PROTO((void));
extern int input_buf PROTO((void));
extern int input_komp PROTO((void));
extern int input_std PROTO((void));
#else
extern void switch_to_stdin PROTO((void));
extern void switch_to_char_buffer PROTO((void));
extern int switch_to_buffer PROTO((void));
extern int terminate_buffer PROTO((void));
#endif

extern void set_new_name PROTO((char *));
extern int init_input PROTO((int, char *[]));

#endif /* _INPUT_H */
