/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992 - 1998 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
/*
 * sigig_posix.c
 *
 * Ignores interrupts in a fork-ed process.
 * It is used from rw.c file.
 */

#include <stdio.h>
#include <signal.h>

static struct sigaction act, oact;

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

void mysigignore PROTO((void));

void mysigignore()
{
 act.sa_handler = SIG_IGN;
 sigemptyset(&act.sa_mask);
 act.sa_flags = 0;
 if (sigaction (SIGINT, &act, &oact) < 0)
   perror ("sigaction");
}
