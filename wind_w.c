/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * wind_w.c
 *
 * Alphanumerical window opening.
 */
#include <X11/Xlib.h>


#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

extern char *callocx PROTO((unsigned, unsigned));
extern void error_message PROTO((int));


XGCValues value;
XEvent event;


char **list;
char *path;
int *act_cnt;

int wind_w PROTO((void));

int
wind_w ()
{
#ifdef FORK_YES
  char arg1[32], arg2[32];
#endif

  if ((disp = XOpenDisplay (NULL)) == NULL)
    {
      printf (" XOpen fails write\n");
      return (-1);
    }

#if 0
  XSynchronize(disp,1);
#endif

  root = XDefaultRootWindow (disp);
  screen = XDefaultScreen (disp);

#if 0
  if (!XGetWindowAttributes (disp, root, &root_window_att))
    {
      printf ("get window att failed \n");
      exit (0);
    }

  XSynchronize (disp, 1);
#endif

  width = XDisplayWidth (disp, screen);
#if 0
  XSynchronize (disp, 1);
#endif
  height = XDisplayHeight (disp, screen);
#if 0
  XSynchronize (disp, 1);
  printf ("w: %d, h: %d \n", width, height);

  default_gc = XDefaultGC (disp, screen);
  XSynchronize (disp, 1);

  XSetState (disp, default_gc, 0xffffffL, 0L, GXcopy, 0xffffffffL);
  XSynchronize (disp, 1);
  printf ("xsetstate\n");
#endif



  width_w = 400;
  height_w = (((int)(channel[channel_handle].fields/5)) + 1) * 15;



  /* 
   * Window attributes.
   */
  mywin_att.background_pixmap = None;
  mywin_att.background_pixel = 0L;
  mywin_att.border_pixmap = CopyFromParent;
  mywin_att.border_pixel = 1L;
  mywin_att.bit_gravity = ForgetGravity;
  mywin_att.win_gravity = NorthWestGravity;
  mywin_att.backing_store = Always;
  mywin_att.backing_planes = 0xffffffffL;
  mywin_att.backing_pixel = 0L;
  mywin_att.save_under = True;
  mywin_att.event_mask = KeyPressMask|ExposureMask|ButtonPressMask|VisibilityChangeMask|SubstructureNotifyMask|StructureNotifyMask;
  mywin_att.do_not_propagate_mask = NoEventMask;
  mywin_att.override_redirect = False;
  mywin_att.colormap = CopyFromParent;
  mywin_att.cursor = None;


  /* 
   * Creates a window.
   */

  channel[channel_handle].mywin_write
    = XCreateWindow (disp, root, x, y, width_w, height_w, border,
		     CopyFromParent, InputOutput, CopyFromParent,
		     CWBackPixel|CWSaveUnder|CWBackingStore|CWEventMask|CWBackingPlanes|CWDontPropagate,
		     &mywin_att);


#ifdef FORK_YES
  /* 
   * Creates a pixmap.
   * It is used when the exposure event occurs.
   */
  channel[channel_handle].mypix_write
    = XCreatePixmap (disp, channel[channel_handle].mywin_write,
		     width_w, height_w, XDefaultDepth (disp, screen));
#endif
#if 0
  XSynchronize (disp, 1);
  printf ("xcreatewritewindow\n");
#endif

  value.background = 0L;
  value.graphics_exposures = True;
  value.foreground = 0xffffffL;

  set_gc_write = XCreateGC (disp, channel[channel_handle].mywin_write,
			    GCForeground|GCBackground|GCGraphicsExposures,
			    &value);



  XClearWindow (disp, channel[channel_handle].mywin_write);
#ifdef FORK_YES
  XFillRectangle (disp, channel[channel_handle].mypix_write,
		  default_gc, 0, 0,
		  channel[channel_handle].w_resolution[0],
		  channel[channel_handle].w_resolution[1]);
#endif
#if 0
  XSynchronize (disp, 1);
#endif

/*
 * Name of the window.
 */
  sprintf (window_name_tmp, "%d", channel_handle);
  strcat (window_name_wr_cur, window_name_tmp);
  XStoreName (disp, channel[channel_handle].mywin_write,
	      window_name_wr_cur);
  strcpy (window_name_wr_cur, window_name_wr_init);
#if 0
  XSynchronize (disp, 1);
  printf ("xstowriterename\n");
#endif

  XMapWindow (disp, channel[channel_handle].mywin_write);
#if 0
  XSynchronize (disp, 1);
  printf ("xmapwritewindow\n");


  XSynchronize (disp, 1);


  channel[channel_handle].item.chars=" ";
  channel[channel_handle].item.nchars
    = strlen (channel[channel_handle].item.chars);
#endif

  channel[channel_handle].item.delta = 1;
  channel[channel_handle].item.font = None;

#if 0
  printf ("item filled\n");
#endif

/*
 * Managing fonts.
 */
  XSetFontPath (disp, &path, 0);
#if 0 
  printf ("xsetfontpath\n");
#endif

  if ((act_cnt = (int *) callocx (1, sizeof(int))) == NULL)
    {
      error_message (4002);
      return (-3);
    }
  while (*act_cnt == 0)
    list = XListFonts (disp, "*", 10, act_cnt);

#if 0
  printf ("xlistfonts\n");
#endif

  channel[channel_handle].item.font = XLoadFont (disp, list[0]);
#if 0
  printf ("xloadfont\n");
#endif
  channel[channel_handle].myfont
    = XQueryFont (disp, channel[channel_handle].item.font);
#if 0
printf ("xqueryfont\n");
#endif


/*
 * Fork for managers that cannot manage exposure events.
 */
#ifdef FORK_YES
  if ((channel[channel_handle].ch_pid_write = fork ()) < 0)
    printf ("could not fork %d\n", errno);
  else if(channel[channel_handle].ch_pid_write == 0)     /* child */
    {
      sprintf (arg1, "%ld", channel[channel_handle].mywin_write);
      sprintf (arg2, "%ld", channel[channel_handle].mypix_write);
#if 0
      printf ("fork child\n");
#endif
      execl ("rw", "rw", arg1, arg2, (char *)0);
      printf ("errno %d\n", errno);
      printf ("bad exec\n");
    }
      /* parent */
#endif /* FORK_YES */
  return (0);
}
