/* Define default alignment for doubles. */
#undef MAX_ALIGNMENT

/* Define if you want to check stack. */
#undef CHKSTACK

/* Define if you want to generate output from the virtual machine. */
#undef CODE

/* Define if you want to have automatic control enhancements. */
#undef CONTROL

/* Define if you want to use more output during compilation and
   run-time. */
#undef DEBUG

/* Define if you have TIOCSTI macro. */
#undef HAVE_TIOCSTI

/* Define if you have <assert.h>. */
#undef HAVE_ASSERT_H
@TOP@
