/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * clif.c
 *
 * System dependent main function
 */

#include "global.h"

/* CONTROL macro is in the following header. */
#include "config.h"

extern void term_restore PROTO((void));
extern void interrupt_register PROTO((void));
extern void clif PROTO((int, char *[]));

int main PROTO((int, char*[]));

int
main (argc, argv)
  int argc;
  char *argv[];
{
#if !(defined (MSDOS) || !defined (CONTROL))
  interrupt_register ();
#endif
  clif (argc, argv);
#if !(defined (MSDOS) || !defined (CONTROL))
  term_restore ();
#endif
  return 0;
}
