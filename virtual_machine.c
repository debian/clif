/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * virtual_machine.c
 *
 * Virtual stack machine.  
 */

#include <stdio.h>
#include <stdlib.h>

#include "global.h"  /* Global variables. */
#include "config.h"
#include "cast.h"    /* Header of used cast operators. */
#include "instr.h"   /* 
		      * Header of structures.
		      * Defines size of virtual machine instructions.
		      */
#include "input.h"   /* Redefining of input function. */
#include "virtual_machine.h"
#include "printfx.h"
#include "tables.h"
#include "lex_t.h"
#include "dbg-out.h"

#define MASK 3 /* Mask for aligning strings in the stack of
		  temporaries. */

static int exe PROTO((char *));
static void mod_yes PROTO((void));
static void div_yes PROTO((void));
static void divui_yes PROTO((void));
static void divli_yes PROTO((void));
static void divlui_yes PROTO((void));
static void divd_yes PROTO((void));
static void divld_yes PROTO((void));
static void divf_yes PROTO((void));
#if 0
static void divc_yes PROTO((void));
#endif
static void vtrue PROTO((void));
static void vfalse PROTO((void));
static void move_stack_aligned PROTO((char **, int));

#ifndef FLEX_SCANNER
extern int input_std PROTO((void));
#else
extern void switch_to_stdin PROTO((void));
#endif

extern FILE *cop;
extern void (*(f[]))();

extern void store_context PROTO((void));
extern int dbg_symbols;

int 
exec()       /* Code execution. */
{
  int a;
  do
    {
#ifdef DEBUG
      printfx ("\nvirt.pc = %u\n",pc);
#endif
      if (dbg_symbols)
	dbg_print (pc);
      a = exe (pc);
#ifndef NOT_MSWIN_AND_YES_DOS
      if (HANDLER_TEST)
	{
	  store_context ();
	  fprintfx (stderr, "\nclif interrupt level %d\n",
		    CLIF_INTERRUPT_LEVEL_INCREMENT);
	  
	  /* 
	   * Interrupt is accepted only if virtual machine is running.
	   * Virtual machine instruction is not interruptible.
	   * Interrupts are only accepted between two virtual machine
	   * instructions (not in a virtual machine instruction). 
	   */

#ifdef FLEX_SCANNER
	  switch_to_stdin ();
#else
	  input = input_std;
#endif
	  HANDLER_SET;
	  break;
	}
#endif
    }
  while (a == 0);
  if (a == -2) 
    return (0); /* End of interpreter session. */
  else
    return (1);
}

 
static int 
exe (pc1)			/* Simulation of virtual stack machine. */
     char *pc1;                    
{
  int binar, counter;
  unsigned int binaru;
  long int binarl;
  long unsigned int binarlu;
#if 0
  short int binars;
  short unsigned int binarsu;
#endif
  double binard;
  long double binarld;
  float binarf;
#if 0
  char binarc;
  signed char binarcs;
  unsigned char binarcu;
#endif
  /*struct  {*/
  static char *adr_arg[256];	/* Addresses of remote functions. */
  /*	} A;*/

  switch (((struct OPERAND_0_ma *)pc1)->major)
    {
    case MOV: 
      switch (((struct OPERAND_1_mi *)pc1)->minor)
	{
	  /* [ADR] <- [[AST]] */
	case 0: 
	  CCI CS (ast + sizeof (char *)) = CCI CS ast;
#ifdef DEBUG
	  printfx ("MOV to address = %u,ast\n",
		   (CS (ast + sizeof (char *))));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address = %u value %d\n",
		    (CS (ast + sizeof (char *))),
		    CCI CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc +=  sizeof (struct OPERAND_0_mi);
	  break;

	  /* BP <- STACK */
	case 1: 
	  bp = stack;
#ifdef DEBUG
	  printfx ("MOV bp <- stack\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1); 
	  fprintfx (cop, "MOV bp <- stack\n");
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* STACK <- BP */
	case 2:
	  stack = bp;
#ifdef DEBUG
	  printfx ("MOV stack <- bp \n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1); 
	  fprintfx (cop, "MOV stack <- bp\n");
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	/* TMPH <- TMP */
	case 3:
	  tmph = tmp;
#ifdef DEBUG
	  printfx ("MOV tmph <- tmp \n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV tmph <- tmp\n");
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* FRAME <- STACK */
	case 4: 
	  frame = stack;
#ifdef DEBUG
	  printfx ("MOV frame <- stack \n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV frame <- stack\n");
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* STACK <- FRAME */
	case 5: 
	  stack = frame;
#ifdef DEBUG
	  printfx ("MOV stack <- frame \n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV stack <- frame \n");
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	/* [AST] <- [AST] */
	case 6: 
	  ast -= sizeof (char *);
	  CS ast = CS (ast + sizeof (char *));
#ifdef DEBUG
	  printfx ("MOV ast <- ast \n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV ast <- ast \n");
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	/* [AST] <- [[AST]] */
	case 7:
	  CS ast = (void *) CCLI CS ast;
#ifdef DEBUG
	  printfx ("MOV ast <- [ast] \n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV ast <- [ast] \n");
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [AST] <- [AST] */
	case 8:
	  move_stack_aligned (&tmp, -POINTER_SIZE);
	  CCLI tmp = (long int) CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV ast <- [ast] \n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV ast <- [ast] \n");
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [AST] <- [(AST - 1) + [AST]] */
	case 9:
	  CS (ast + sizeof (char *))
	    = (void *) (CS (ast + sizeof (char *)) +  CCI CS ast);
#ifdef DEBUG
	  printfx ("MOV ast <- [(ast - 1) + [ast]] \n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV ast <- [(ast - 1) + [ast]]\n");
#endif
	  ast += sizeof (char *);
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- [[AST]] */
	case 10:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = CCI CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %d\n",
		    CS ast, CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [(AST - 2)] <- [(AST - 1) + [AST]] */
	case 11:
	  CCC (CS (ast + 2 * sizeof (char *)) + CCI CS ast)
	    =  CCC (CS (ast + sizeof (char *)) +  CCI CS ast);
#ifdef DEBUG
	  printfx ("MOV [(ast - 2)] <- [(ast - 1) + [ast]] \n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV [(ast - 2)] <- [(ast - 1) + [ast]]\n");
#endif
	  ast += sizeof (char *);
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [ADR] <- [[AST]] */
	case 12:
	  CCUI CS (ast + sizeof (char *)) = CCUI CS ast;
#ifdef DEBUG
	  printfx ("MOV to address = %u,ast\n",
		   (CS (ast + sizeof (char *))));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address = %u value %u\n",
		    (CS (ast + sizeof (char *))),
		    CCUI CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc +=  sizeof (struct OPERAND_0_mi);
	  break;

	  /* [ADR] <- [[AST]] */
	case 13:
	  CCLI CS (ast + sizeof (char *)) = CCLI CS ast;
#ifdef DEBUG
	  printfx ("MOV to address = %u,ast\n",
		   (CS (ast + sizeof (char *))));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address = %u value %ld\n",
		    (CS (ast + sizeof (char *))),
		    CCLI CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc +=  sizeof (struct OPERAND_0_mi);
	  break;

	  /* [ADR] <- [[AST]] */
	case 14:
	  CCLUI CS (ast + sizeof (char *)) = CCLUI CS ast;
#ifdef DEBUG
	  printfx ("MOV to address = %u,ast\n",
		   (CS (ast + sizeof (char *))));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address = %u value %lu\n",
		    (CS (ast + sizeof (char *))),
		    CCLUI CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc +=  sizeof (struct OPERAND_0_mi);
	  break;

	  /* [ADR] <- [[AST]] */
	case 15:
	  CCSI CS (ast + sizeof (char *)) = CCSI CS ast;
#ifdef DEBUG
	  printfx ("MOV to address = %u,ast\n",
		   (CS (ast + sizeof (char *))));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address = %u value %hd\n",
		    (CS (ast + sizeof (char *))),
		    CCSI CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc +=  sizeof (struct OPERAND_0_mi);
	  break;

	  /* [ADR] <- [[AST]] */
	case 16:
	  CCSUI CS (ast + sizeof (char *)) = CCSUI CS ast;
#ifdef DEBUG
	  printfx ("MOV to address = %u,ast\n",
		   (CS (ast + sizeof (char *))));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address = %u value %hu\n",
		    (CS (ast + sizeof (char *))),
		    CCSUI CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc +=  sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- [[AST]] */
	case 17:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = CCUI CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %u\n",
		    CS ast, CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- [[AST]] */
	case 18:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = CCLI CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %ld\n",
		    CS ast, CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- [[AST]] */
	case 19:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = CCLUI CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %lu\n",
		    CS ast, CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- [[AST]] */
	case 20:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = CCSI CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %hd\n",
		    CS ast, CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- [[AST]] */
	case 21:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = CCSUI CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %hu\n",
		    CS ast, CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [ADR] <- [[AST]] */
	case 256:
	  CCD CS (ast + sizeof (char *)) = CCD CS ast;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n",
		   CS(ast + sizeof (char *)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ",pc1);
	  fprintfx (cop, "MOV to address=%u value %lf\n", 
		    CS (ast + sizeof (char *)),
		    CCD CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- [[AST]] */
	case 257:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = CCD CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %lf\n",
		    CS ast, CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [ADR] <- [[AST]] */
	case 258:
	  CCLD CS (ast + sizeof (char *)) = CCLD CS ast;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n",
		   CS(ast + sizeof (char *)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ",pc1);
	  fprintfx (cop, "MOV to address=%u value %lf\n", 
		    CS (ast + sizeof (char *)),
		    CCLD CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- [[AST]] */
	case 259:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = CCLD CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %lf\n",
		    CS ast, CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [ADR] <- [[AST]] */
	case 512:
	  CCF CS (ast + sizeof (char *)) = CCF CS ast;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n",
		   CS (ast + sizeof (char *)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %lf\n",
		    CS (ast + sizeof (char *)),
		    CCF CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc += sizeof (struct OPERAND_0_mi);
	break;

	  /* [[AST]] <- [[AST]] */
	case 513:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = CCF CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %lf\n",
		    CS ast, CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [ADR] <- [[AST]] */
	case 768: 
	  CCC CS (ast + sizeof (char *)) = CCC CS ast;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n",
		   CS (ast + sizeof (char *)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %d\n",
		    CS (ast + sizeof (char *)),
		    CCC CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc += sizeof (struct OPERAND_0_mi);
	break;

	  /* [[AST]] <- [[AST]] */
	case 769:
	  move_stack_aligned (&tmp, -sizeof (char));
	  CCC tmp = CCC CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %d\n",
		    CS ast, CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [ADR] <- [[AST]] */
	case 770:
	  CCSC CS (ast + sizeof (char *)) = CCSC CS ast;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n",
		   CS (ast + sizeof (char *)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %d\n",
		    CS (ast + sizeof (char *)),
		    CCSC CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc += sizeof (struct OPERAND_0_mi);
	break;

	  /* [ADR] <- [[AST]] */
	case 771:
	  CCUC CS (ast + sizeof (char *)) = CCUC CS ast;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n",
		   CS (ast + sizeof (char *)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %u\n",
		    CS (ast + sizeof (char *)),
		    CCUC CS (ast + sizeof (char *)));
#endif
	  ast += sizeof (char *);
	  pc += sizeof (struct OPERAND_0_mi);
	break;

	  /* [[AST]] <- [[AST]] */
	case 772:
	  move_stack_aligned (&tmp, -sizeof (signed char));
	  CCSC tmp = CCSC CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %d\n",
		    CS ast, CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- [[AST]] */
	case 773:
	  move_stack_aligned (&tmp, -sizeof (unsigned char));
	  CCUC tmp = CCUC CS ast;
	  ast -= sizeof (char *);
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("MOV to address=%u,ast\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MOV to address=%u value %u\n",
		    CS ast, CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	default : 
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;
      

      /* TMP <- TMPH */

    case CLRT: 
      tmp = tmph;
      pc += sizeof (struct OPERAND_0_ma);
#ifdef DEBUG
      printfx ("temporary stack cleared");
#endif
#ifdef CODE
      fprintfx (cop,"\n%u   ",pc1); fprintfx (cop,"CLRT\n");
#endif
      break;
    case CVT: 
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* integer -> double */
	case 0: 
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("integer converted to double on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT integer to double on the top %lf\n", 
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* integer -> double */
	case 1: 
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("integer converted to double at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT integer to double at the bottom %lf\n",
		    CCD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* double -> integer */
	case 2: 
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("double converted to integer\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT double to integer %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* integer -> float */
	case 3: 
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("integer converted to float on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT integer to float on the top %g\n",
		    CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* integer -> float */
	case 4: 
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("integer converted to float at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT integer to float at the bottom %g\n",
		    CCF CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* float -> integer */
	case 5: 
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("float converted to integer\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT float to integer %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* float -> double */
	case 6: 
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("float converted to double on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT float to double on the top %lf\n",
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* float -> double */
	case 7:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCF CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("float converted to double at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT float to double at the bottom %lf\n",
		    CCD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* double -> float */
      case 8: 
	move_stack_aligned (&tmp, -sizeof (float));
	CCF tmp = (float)CCD CS ast;
	CS ast = tmp;
#ifdef DEBUG
	printfx ("double converted to float\n");
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "CVT double to float %g\n", CCF CS ast);
#endif
	pc += sizeof (struct OPERAND_0_mi);
	break;

	/* char -> integer */
      case 9:
	move_stack_aligned (&tmp, -sizeof (int));
	CCI tmp = (int)CCC CS ast;
	CS ast = tmp;
#ifdef DEBUG
	printfx ("char converted to integer on the top\n");
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "CVT char to integer on the top %d\n",
		  CCI CS ast); 
#endif
	pc += sizeof (struct OPERAND_0_mi);
	break;

	/* char -> integer */
	case 10:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("char converted to integer at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT char to integer at the botttom %d\n", 
		    CCI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* integer -> char */
	case 11:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCC tmp = (char)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("integer converted to char\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT integer to char %d\n", 
		    CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* char -> double */
	case 12:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("char converted to double on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT char to double on the top %lf\n",
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* char -> double */
	case 13:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("char converted to double at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT char to double at the bottom %lf\n",
		    CCD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* double -> char */
	case 14:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCC tmp = (char)CCD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("double converted to char\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT double to char %d\n",
		    CCC CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* char -> float */
	case 15:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("char converted to float\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT char to float on the top %g\n",
		    CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* char -> float */
	case 16:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("char converted to float at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT char to float at the bottom %g\n",
		    CCF CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* float -> char */
	case 17:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCC tmp = (char)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("float converted to char\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT float to char %d\n", CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> double */
	case 18:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `double'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `double' on the top %g\n",
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> double */
	case 19:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `double' at the bottom %g\n",
		    CCD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> float */
	case 20:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `float' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `float' on the top %g\n",
		    CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> float */
	case 21:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `float' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `float' at the bottom %g\n",
		    CCF CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> long int */
	case 22:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `long int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `long int' on the top %ld\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> long int */
	case 23:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `long int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `long int' at the bottom %ld\n",
		    CCLI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef UINT_EQ_ULONG
	  /* unsigned int -> long unsigned int */
	case 24:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `long unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `long unsigned int' on the top %lu\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> long unsigned int */
	case 25:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp =
	    (long unsigned int)CCUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `long unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `long unsigned int' at the bottom %lu\n",
		    CCLUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* unsigned int -> int */
	case 26:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `int' %d\n",
		    CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef SHORT_EQ_INT
	  /* unsigned int -> short int */
	case 27:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `short int' %hd\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_USHORT
	  /* unsigned int -> short unsigned int */
	case 28:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* unsigned int -> char */
	case 29:
	  move_stack_aligned (&tmp, -sizeof (char));
	  CCC tmp = (char)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `char' %d\n",
		    CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> signed char */
	case 30:
	  move_stack_aligned (&tmp, -sizeof (signed char));
	  CCSC tmp = (signed char)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `signed char' %d\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> signed char */
	case 31:
	  move_stack_aligned (&tmp, -sizeof (unsigned char));
	  CCUC tmp = (unsigned char)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `unsigned char' %d\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef LONG_EQ_INT
	  /* long int -> double */
	case 32:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `double' %g on the top\n",
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> double */
	case 33:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCLI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `double' %g at the bottom\n",
		    CCD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> float */
	case 34:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `float' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `float' %g on the top\n",
		    CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> float */
	case 35:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCLI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `float' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `float' %g at the bottom\n",
		    CCF CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> long unsigned int */
	case 36:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `long unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `long unsigned int' %lu on the top\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> float */
	case 37:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp 
	    = (long unsigned int)CCLI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `long unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `long unsigned int' %lu at the bottom\n",
		    CCLUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> int */
	case 38:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `int' %d \n",
		    CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> unsigned int */
	case 39:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `unsigned int' %u\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef SHORT_EQ_INT
	  /* long int -> short int */
	case 40:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `short int' %hd\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* long int -> short unsigned int */
	case 41:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> char */
	case 42:
	  move_stack_aligned (&tmp, -sizeof (char));
	  CCC tmp = (char)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `char' %d\n",
		    CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> signed char */
	case 43:
	  move_stack_aligned (&tmp, -sizeof (signed char));
	  CCSC tmp = (signed char)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `signed char' %d\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> unsigned char */
	case 44:
	  move_stack_aligned (&tmp, -sizeof (unsigned char));
	  CCUC tmp = (unsigned char)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `unsigned char' %d\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_ULONG
	  /* long unsigned int -> double */
	case 45:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `double' %g on the top\n",
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> double */
	case 46:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp 
	    = (double)CCLUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `double' %g at the bottom\n",
		    CCD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> float */
	case 47:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `float' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `float' %g on the top\n",
		    CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> float */
	case 48:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp 
	    = (float)CCLUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `float' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `float' %g at the bottom\n",
		    CCF CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> long int */
	case 49:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `long int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `long int' %ld\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> unsigned int */
	case 50:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `unsigned int' %u\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> int */
	case 51:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `int' %d\n",
		    CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> short unsigned int */
	case 52:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> short int */
	case 53:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `short int' %hd\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> char */
	case 54:
	  move_stack_aligned (&tmp, -sizeof (char));
	  CCC tmp = (char)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `char' %d\n",
		    CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> signed char */
	case 55:
	  move_stack_aligned (&tmp, -sizeof (signed char));
	  CCSC tmp = (signed char)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `signed char' %d\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> unsigned char */
	case 56:
	  move_stack_aligned (&tmp, -sizeof (unsigned char));
	  CCUC tmp = (unsigned char)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `unsigned char' %d\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef SHORT_EQ_INT
	  /* short int -> double */
	case 57:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `double' %g on the top\n",
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> double */
	case 58:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp 
	    = (double)CCSI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `double' %g at the bottom\n",
		    CCD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> float */
	case 59:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `float' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `float' %g on the top\n",
		    CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> float */
	case 60:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp 
	    = (float)CCSI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `float' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `float' %g at the bottom\n",
		    CCF CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef LONG_EQ_INT
	  /* short int -> long int */
	case 61:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `long int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `long int' %ld on the top\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> long int */
	case 62:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp 
	    = (long int)CCSI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `long int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `long int' %ld at the bottom\n",
		    CCLI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* short int -> long unsigned int */
	case 63:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `long unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `long unsigned int' %lu on the top\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> long unsigned int */
	case 64:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp 
	    = (long unsigned int)CCSI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `long unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `long unsigned int' %lu at the bottom\n",
		    CCLUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> int */
	case 65:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `int' %d on the top\n",
		    CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> int */
	case 66:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp 
	    = (int)CCSI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `int' %d at the bottom\n",
		    CCI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> unsigned int */
	case 67:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `unsigned int' %u on the top\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> unsigned int */
	case 68:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp 
	    = (unsigned int)CCSI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop,
		    "CVT `short int' to `unsigned int' %u at the bottom\n",
		    CCUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> short unsigned int */
	case 69:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> char */
	case 70:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCC tmp = (char)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `char' %d\n",
		    CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> unsigned char */
	case 71:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCUC tmp = (unsigned char)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `unsigned char' %u\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> signed char */
	case 72:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCSC tmp = (signed char)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `signed char' %d\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_USHORT
	  /* short unsigned  int -> double */
	case 73:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `double' %g on the top\n",
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> double */
	case 74:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp 
	    = (double)CCSUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `double' %g at the bottom\n",
		    CCD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> float */
	case 75:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `float' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `float' %g on the top\n",
		    CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> float */
	case 76:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp 
	    = (float)CCSUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `float' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `float' %g at the bottom\n",
		    CCF CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> long int */
	case 77:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `long int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `long int' %ld on the top\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> long int */
	case 78:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp 
	    = (long int)CCSUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `long int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, 
		    "CVT `short unsigned int' to `long int' %ld at the bottom\n",
		    CCLI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> long unsigned int */
	case 79:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `long unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `long unsigned int' %lu on the top\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> long unsigned int */
	case 80:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp 
	    = (long unsigned int)CCSUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `long unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `long unsigned int' %lu at the bottom\n",
		    CCLUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> int */
	case 81:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `int' %d on the top\n",
		    CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> int */
	case 82:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp 
	    = (int)CCSUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `int' %d at the bottom\n",
		    CCI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> unsigned int */
	case 83:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `unsigned int' %u on the top\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> unsigned int */
	case 84:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp 
	    = (unsigned int)CCSUI CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop,
		    "CVT `short unsigned int' to `unsigned int' %u at the bottom\n",
		    CCUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> short int */
	case 85:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `short int' %hu\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> char */
	case 86:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCC tmp = (char)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `char' %d\n",
		    CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> unsigned char */
	case 87:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCUC tmp = (unsigned char)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `unsigned char' %u\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> signed char */
	case 88:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCSC tmp = (signed char)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `signed char' %d\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef LONG_EQ_INT
	  /* char -> long int */
	case 89:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `long int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `long int' %ld on the top\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* char -> long int */
	case 90:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `long int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `long int' %ld at the bottom\n",
		    CCLI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_ULONG
	  /* char -> long unsigned int */
	case 91:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `long unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `long unsigned int' %lu on the top\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* char -> long unsigned int */
	case 92:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `long unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `long unsigned int' %lu at the bottom\n",
		    CCLUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* char -> unsigned int */
	case 93:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `unsigned int' %u on the top\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* char -> unsigned int */
	case 94:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `unsigned int' %u at the bottom\n",
		    CCUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef SHORT_EQ_INT
	  /* char -> short int */
	case 95:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCC CS ast;
	  CS ast  = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `short int' %hd\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_USHORT
	  /* char -> short unsigned int */
	case 96:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif
	  /* char -> signed char */
	case 97:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCSC tmp = (signed char)CCC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `signed char' %d\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* char -> unsigned char */
	case 98:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCUC tmp = (unsigned char)CCC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `unsigned char' %u\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> double */
	case 99:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `double' %g on the top\n",
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> double */
	case 100:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCSC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `double' %g at the bottom\n",
		    CCD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> float */
	case 101:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `float' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `float' %g on the top\n",
		    CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> float */
	case 102:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCSC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `float' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `float' %g at the bottom\n",
		    CCF CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef LONG_EQ_INT
	  /* signed char -> long int */
	case 103:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `long int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `long int' %ld on the top\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> long int */
	case 104:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCSC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `long int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `long int' %ld at the bottom\n",
		    CCLI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_ULONG
	  /* signed char -> long unsigned int */
	case 105:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `long unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `long unsigned int' %lu on the top\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> long unsigned int */
	case 106:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp =
	    (long unsigned int)CCSC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `long unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `long unsigned int' %lu at the bottom\n",
		    CCLUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* signed char -> int */
	case 107:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `int' %d on the top\n",
		    CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> int */
	case 108:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCSC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `int' %d at the bottom\n",
		    CCI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> unsigned int */
	case 109:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `unsigned int' %u on the top\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> unsigned int */
	case 110:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCSC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `unsigned int' %u at the bottom\n",
		    CCUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef SHORT_EQ_INT
	  /* signed char -> short int */
	case 111:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `short int' %hd\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_USHORT
	  /* signed char -> short unsigned int */
	case 112:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* signed char -> char */
	case 113:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCC tmp = (char)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `char' %d\n",
		    CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> unsigned char */
	case 114:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCUC tmp = (unsigned char)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `unsigned char' %d\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> double */
	case 115:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `double' %g on the top\n",
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> double */
	case 116:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCUC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `double' %g at the bottom\n",
		    CCD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> float */
	case 117:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `float' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `float' %g on the top\n",
		    CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> float */
	case 118:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCUC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `float' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `float' %g at the bottom\n",
		    CCF CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef LONG_EQ_INT
	  /* unsigned char -> long int */
	case 119:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `long int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `long int' %ld on the top\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> long int */
	case 120:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCUC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `long int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `long int' %ld at the bottom\n",
		    CCLI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_ULONG
	  /* unsigned char -> long unsigned int */
	case 121:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `long unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `long unsigned int' %lu on the top\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> long unsigned int */
	case 122:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp =
	    (long unsigned int)CCUC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `long unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `long unsigned int' %lu at the bottom\n",
		    CCLUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* unsigned char -> int */
	case 123:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `int' %d on the top\n",
		    CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> int */
	case 1124:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCUC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `int' %d at the bottom\n",
		    CCI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> unsigned int */
	case 125:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `unsigned int' %u on the top\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> unsigned int */
	case 126:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCUC CS (ast + sizeof (char *));
	  CS (ast + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `unsigned int' %u at the bottom\n",
		    CCUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef SHORT_EQ_INT
	  /* unsigned char -> short int */
	case 127:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `short int' %hd\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_USHORT
	  /* unsigned char -> short unsigned int */
	case 128:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* unsigned char -> char */
	case 129:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCC tmp = (char)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `char' %d\n",
		    CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> signed char */
	case 130:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCSC tmp = (signed char)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `signed char' %d\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef LONG_EQ_INT
	  /* double -> long int */
	case 131:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`double' converted to `long int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `double' to `long int' %ld\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_ULONG
	  /* double -> long unsigned int */
	case 132:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`double' converted to `long unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `double' to `long unsigned int' %lu\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* double -> unsigned int */
	case 133:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`double' converted to `unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `double' to `unsigned int' %u\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef SHORT_EQ_INT
	  /* double -> short int */
	case 134:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`double' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `double' to `short int' %hd\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_USHORT
	  /* double -> short unsigned int */
	case 135:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`double' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `double' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* double -> signed char */
	case 136:
	  move_stack_aligned (&tmp, -sizeof (signed char));
	  CCSC tmp = (signed char)CCD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`double' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `double' to `signed char' %d\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* double -> unsigned char */
	case 137:
	  move_stack_aligned (&tmp, -sizeof (unsigned char));
	  CCUC tmp = (unsigned char)CCD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`double' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `double' to `unsigned char' %u\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef LONG_EQ_INT
	  /* float -> long int */
	case 138:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`float' converted to `long int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `float' to `long int' %ld\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_ULONG
	  /* float -> long unsigned int */
	case 139:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`float' converted to `long unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `float' to `long unsigned int' %lu\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* float -> unsigned int */
	case 140:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`float' converted to `unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `float' to `unsigned int' %u\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef SHORT_EQ_INT
	  /* float -> short int */
	case 141:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`float' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `float' to `short int' %hd\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_USHORT
	  /* float -> short unsigned int */
	case 142:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`float' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `float' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* float -> signed char */
	case 143:
	  move_stack_aligned (&tmp, -sizeof (signed char));
	  CCSC tmp = (signed char)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`float' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `float' to `signed char' %d\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* float -> unsigned char */
	case 144:
	  move_stack_aligned (&tmp, -sizeof (unsigned char));
	  CCUC tmp = (unsigned char)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`float' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `float' to `unsigned char' %u\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef LONG_EQ_INT
	  /* int -> long int */
	case 145:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `long int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `long int' %ld on the top\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* int -> long int */
	case 146:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCI CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `long int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `long int' %ld at the bottom\n",
		    CCLI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_ULONG
	  /* int -> long unsigned int */
	case 147:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `long unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `long unsigned int' %lu on the top\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* int -> long unsigned int */
	case 148:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp =
	    (long unsigned int)CCI CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `long unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `long unsigned int' %lu at the bottom\n",
		    CCLUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* int -> unsigned int */
	case 149:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `unsigned int' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `unsigned int' %u on the top\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* int -> unsigned int */
	case 150:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp =
	    (unsigned int)CCI CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `unsigned int' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `unsigned int' %u at the bottom\n",
		    CCUI CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef SHORT_EQ_INT
	  /* int -> short int */
	case 151:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `short int' %hd\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_USHORT
	  /* int -> short unsigned int */
	case 152:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* int -> signed char */
	case 153:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCSC tmp = (signed char)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `signed char' %u\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* int -> unsigned char */
	case 154:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCUC tmp = (unsigned char)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `unsigned char' %u\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef LDOUBLE_EQ_DOUBLE
	  /* long double -> double */
	case 155:
	  move_stack_aligned (&tmp, -sizeof (double));
	  CCD tmp = (double)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `double'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `double' %g\n",
		    CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long double -> float */
	case 156:
	  move_stack_aligned (&tmp, -sizeof (float));
	  CCF tmp = (float)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `float'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `float' %g\n",
		    CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef LONG_EQ_INT
	  /* long double -> long int */
	case 157:
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CCLI tmp = (long int)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `long int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `long int' %ld\n",
		    CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_ULONG
	  /* long double -> long unsigned int */
	case 158:
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CCLUI tmp = (long unsigned int)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `long unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `long unsigned int' %lu\n",
		    CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* long double -> int */
	case 159:
	  move_stack_aligned (&tmp, -sizeof (int));
	  CCI tmp = (int)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `int' %d\n",
		    CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long double -> unsigned int */
	case 160:
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CCUI tmp = (unsigned int)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `unsigned int' %d\n",
		    CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef SHORT_EQ_INT
	  /* long double -> short int */
	case 161:
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CCSI tmp = (short int)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `short int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `short int' %hd\n",
		    CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_USHORT
	  /* long double -> short unsigned int */
	case 162:
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CCSUI tmp = (short unsigned int)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `short unsigned int'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `short unsigned int' %hu\n",
		    CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* long double -> char */
	case 163:
	  move_stack_aligned (&tmp, -sizeof (char));
	  CCC tmp = (char)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `char' %d\n",
		    CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long double -> unsigned char */
	case 164:
	  move_stack_aligned (&tmp, -sizeof (unsigned char));
	  CCUC tmp = (unsigned char)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `unsigned char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `unsigned char' %u\n",
		    CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long double -> signed char */
	case 165:
	  move_stack_aligned (&tmp, -sizeof (signed char));
	  CCSC tmp = (signed char)CCLD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long double' converted to `signed char'\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long double' to `signed char' %d\n",
		    CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* int -> long double */
	case 166:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* int -> long double */
	case 167:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCI CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`int' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `int' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> long double */
	case 168:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned int -> long double */
	case 169:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCUI CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned int' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned int' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#ifndef LONG_EQ_INT
	  /* long int -> long double */
	case 170:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCLI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long int -> long double */
	case 171:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCLI CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`long int' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long int' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef SHORT_EQ_INT
	  /* short int -> long double */
	case 172:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCSI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short int -> long double */
	case 173:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCSI CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short int' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short int' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_ULONG
	  /* long unsigned int -> long double */
	case 174:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCLUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* long unsigned int -> long double */
	case 175:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCLUI CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`long unsigned int' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `long unsigned int' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

#ifndef UINT_EQ_USHORT
	  /* short unsigned int -> long double */
	case 176:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCSUI CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* short unsigned int -> long double */
	case 177:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCSUI CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`short unsigned int' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `short unsigned int' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* double -> long double */
	case 178:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCD CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`double' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `double' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* double -> long double */
	case 179:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCD CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`double' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `double' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* float -> long double */
	case 180:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCF CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`float' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `float' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* float -> long double */
	case 181:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCF CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`float' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `float' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* char -> long double */
	case 182:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* char -> long double */
	case 183:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCC CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`char' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `char' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> long double */
	case 184:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCSC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* signed char -> long double */
	case 185:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCSC CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`signed char' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `signed char' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> long double */
	case 186:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp = (long double)CCUC CS ast;
	  CS ast = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `long double' on the top\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `long double' %g on the top\n",
		    CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* unsigned char -> long double */
	case 187:
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CCLD tmp =
	    (long double)CCUC CS (ast + sizeof (char *));
	  CS (ast  + sizeof (char *)) = tmp;
#ifdef DEBUG
	  printfx ("`unsigned char' converted to `long double' at the bottom\n");
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CVT `unsigned char' to `long double' %g at the bottom\n",
		    CCLD CS (ast + sizeof (char *)));
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;
    case PUSHA:
      switch (((struct OPERAND_1_mi *)pc1)->minor)
	{
	  /* [AST] <- [ADR] */
	case 0:
	  ast -= sizeof (char *);
	  CS ast = (((struct OPERAND_1_mi *)pc1)->adr);
#ifdef DEBUG
	  printfx ("astu=%u\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHA to address %u\n", CS ast);
	  /*	fprintfx (cop,"value of ast %x\n",ast);*/
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;
	
	  /* [AST] <- [NUM+BP] */
	case 1:
	  ast -= sizeof (char *);
	  CS ast = (((struct OPERAND_1_i *)pc1)->num + bp);
#ifdef DEBUG
	  printfx ("PUSHA abs. address=%u\n",
		   (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHA to abs. address %u\n",
		    (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* [AST] <- [[NUM+BP]] */
	case 2:
	  ast -= sizeof (char *);
	  CS ast = CS (((struct OPERAND_1_i *)pc1)->num + bp);
#ifdef DEBUG
	  printfx ("PUSHA abs. address=%u\n", CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHA to abs. address %u\n", CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;
    case POPA:
      switch (((struct OPERAND_1_mi *)pc1)->minor)
	{
	  /* POPA empty - the stack is cleared */
	case 0:
	  ast += sizeof (char *);
	  pc += sizeof (struct OPERAND_0_mi);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "POPA\n");
#endif
	  break;
      case 1:
	CS (((struct OPERAND_1_i *)pc)->num + stack) = CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%x\n", 
		 CS (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %x\n",
		  CS (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;
      case 2:
	CCI (((struct OPERAND_1_i *)pc)->num + stack) = CCI CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%d\n", 
		 CCI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %d\n",
		  CCI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;
      case 3:
	CCUI (((struct OPERAND_1_i *)pc)->num + stack) = CCUI CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%ud\n", 
		 CCUI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %ud\n",
		  CCUI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;
      case 4:
	CCLI (((struct OPERAND_1_i *)pc)->num + stack) = CCLI CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%ld\n", 
		 CCLI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %ld\n",
		  CCLI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;
      case 5:
	CCLUI (((struct OPERAND_1_i *)pc)->num + stack) = CCLUI CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%lud\n", 
		 CCLUI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %lud\n",
		  CCLUI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;
      case 6:
	CCSI (((struct OPERAND_1_i *)pc)->num + stack) = CCSI CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%hd\n", 
		 CCSI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %hd\n",
		  CCSI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;
      case 7:
	CCSUI (((struct OPERAND_1_i *)pc)->num + stack) = CCSUI CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%hud\n", 
		 CCSUI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %hud\n",
		  CCSUI (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;

      case 256:
	CCD (((struct OPERAND_1_i *)pc)->num + stack) = CCD CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%g\n", 
		 CCD (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %g\n",
		  CCD (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;
      case 257:
	CCLD (((struct OPERAND_1_i *)pc)->num + stack) = CCLD CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%Lg\n", 
		 CCLD (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %Lg\n",
		  CCLD (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;

      case 512:
	CCF (((struct OPERAND_1_i *)pc)->num + stack) = CCF CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%g\n", 
		 CCF (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %g\n",
		  CCF (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;

      case 768:
	CCC (((struct OPERAND_1_i *)pc)->num + stack) = CCC CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%c\n", 
		 CCC (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %c\n",
		  CCC (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;
      case 769:
	CCSC (((struct OPERAND_1_i *)pc)->num + stack) = CCSC CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%c\n", 
		 CCSC (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %c\n",
		  CCSC (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;
      case 770:
	CCUC (((struct OPERAND_1_i *)pc)->num + stack) = CCUC CS ast;
	ast += sizeof (char *);
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%uc\n", 
		 CCUC (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %uc\n",
		  CCUC (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;
      case 771:
	CCC (((struct OPERAND_1_i *)pc)->num + stack)
	  = CCC (CS ast + (((struct OPERAND_1_i *)pc)->num));
#ifdef DEBUG
	printfx ("POPA to stack\n");
	printfx ("value=%c\n", 
		 CCC (((struct OPERAND_1_i *)pc)->num + stack));
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "POPA to stack value %c\n",
		  CCC (((struct OPERAND_1_i *)pc)->num + stack));
#endif
	pc += sizeof (struct OPERAND_1_i);
	break;

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case PUSH:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /*
	    case 0 : stack -= sizeof (char *);
	    check_stack();
	    CS stack = CS ast;
#ifdef DEBUG
printfx ("PUSH address from ast\n");
printfx ("value=%u\n",CS stack);
#endif
#ifdef CODE
fprintfx (cop,"\n%u   ",pc1); fprintfx (cop,"PUSH address %u from ast\n",CS stack);
#endif
ast += sizeof (char *);
pc += sizeof (struct OPERAND_0_mi);
break;
*/

	  /* [STACK] <- BP */
	case 1:
	  stack -= sizeof (char *);
	  check_stack ();
	  CS stack = bp;
#ifdef DEBUG
	  printfx ("PUSH address from bp\n");
	  printfx ("value = %u\n", CS stack);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSH address %u from bp\n", CS stack);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

/*
   case 2 : stack -= sizeof (char *);
   check_stack();
   CS stack=((struct OPERAND_1_mi *)pc1)->adr;
#ifdef DEBUG
printfx ("PUSH address\n");
printfx ("value=%u\n",CS stack);
#endif
#ifdef CODE
fprintfx (cop,"\n%u   ",pc1); fprintfx (cop,"PUSH address %u\n",CS stack);
#endif
pc += sizeof (struct OPERAND_1_mi);
break;
*/

	  /* [STACK] <- TMPH */
      case 3:
	stack -= sizeof (char *);
	check_stack ();
	CS stack = tmph;
#ifdef DEBUG
	printfx ("PUSH tmph\n");
	printfx ("value = %u\n", CS stack);
#endif
#ifdef CODE
	fprintfx (cop, "\n%u   ", pc1);
	fprintfx (cop, "PUSH tmph %u\n", CS stack);
#endif
	pc += sizeof (struct OPERAND_0_mi);
	break;

	/* [STACK] <- FRAME */
	case 4:
	  stack -= sizeof (char *);
	  check_stack ();
	  CS stack = frame;
#ifdef DEBUG
	  printfx ("PUSH frame\n");
	  printfx ("value = %u\n", CS stack);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSH frame %u\n", CS stack);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case POP:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* BP <- [STACK] */
	case 0:
	  bp = CS stack;
	  stack += sizeof (char *);
#ifdef DEBUG
	  printfx ("POP to bp value %u\n", bp);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "POP to bp value %u\n", bp);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* TMPH <- [STACK] */
	case 1:
	  tmph = CS stack;
	  stack += sizeof (char *);
#ifdef DEBUG
	  printfx ("POP to tmph value %u\n", tmph);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "POP to tmph value %u\n", tmph);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* FRAME <- [STACK] */
	case 2:
	  frame = CS stack;
	  stack += sizeof (char *);
#ifdef DEBUG
	  printfx ("POP to frame value %u\n", frame);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "POP to frame value %u\n", frame);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;
      
      /* [ADR_STACK] <- [AST] */
      /*	 case PUSHAD:	adr_stack -= sizeof (char *);
       *adr_stack= CS ast;
       ast += sizeof (char *);
#ifdef DEBUG
printfx ("PUSHAD ast\n");
printfx ("value=%u\n",*adr_stack);
#endif
#ifdef CODE
fprintfx (cop,"\n%u   ",pc1); fprintfx (cop,"PUSHAD ast value %u\n",*adr_stack);
#endif
pc += sizeof (struct OPERAND_0_ma);
break;

*/
      /* [STACK] <- [ADR_STACK] */
      /*	 case POPAD : stack -= sizeof (char *);
		 check_stack();
		 CS stack= *adr_stack;
		 adr_stack += sizeof (char *);
#ifdef DEBUG
printfx ("POPAD to stack");
#endif
#ifdef CODE
fprintfx (cop,"\n%u   ",pc1); fprintfx (cop,"POPAD to stack %u\n",CS stack);
#endif
pc += sizeof (struct OPERAND_0_ma);	
break;
*/

    case ADD:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 0:
	  binar = CCI CS ast + CCI CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar; 
#ifdef DEBUG
	  printfx ("result = %d\n", CCI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* STACK <- STACK-NUM */
	case 1:
	  stack += ((struct OPERAND_1_i *)pc1)->num;
#ifdef DEBUG
	  printfx ("ADD to stack\n");
	  printfx ("value = %u\n", stack);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD to stack, result %u\n", stack);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 2:
	  binaru = CCUI CS ast + CCUI CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CS ast = tmp;
	  CCUI CS ast = binaru; 
#ifdef DEBUG
	  printfx ("result = %u\n", CCUI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %u\n", CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 3:
	  binarl = CCLI CS ast + CCLI CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CS ast = tmp;
	  CCLI CS ast = binarl;
#ifdef DEBUG
	  printfx ("result = %ld\n", CCLI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %ld\n", CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 4:
	  binars = CCSI CS ast + CCSI CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CS ast = tmp;
	  CCSI CS ast = binars;
#ifdef DEBUG
	  printfx ("result = %d\n", CCSI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %d\n", CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 5:
	  binarlu = CCLUI CS ast + CCLUI CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CS ast = tmp;
	  CCLUI CS ast = binarlu;
#ifdef DEBUG
	  printfx ("result = %u\n", CCLUI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %u\n", CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 6:
	  binarsu = CCSUI CS ast + CCSUI CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CS ast = tmp;
	  CCSUI CS ast = binarsu;
#ifdef DEBUG
	  printfx ("result = %u\n", CCSUI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %u\n", CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 256:
	  binard = CCD CS ast + CCD CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (double));
	  CS ast = tmp;
	  CCD CS ast = binard; 
#ifdef DEBUG
	  printfx ("result = %g\n", CCD CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %lf\n", CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 257:
	  binarld = CCLD CS ast + CCLD CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CS ast = tmp;
	  CCLD CS ast = binarld;
#ifdef DEBUG
	  printfx ("result = %g\n", CCLD CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %lf\n", CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 512:
	  binarf = CCF CS ast + CCF CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (float));
	  CS ast = tmp;
	  CCF CS ast = binarf; 
#ifdef DEBUG
	  printfx ("result = %g\n", CCF CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %lf\n", CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 768:
	  binarc = CCC CS ast + CCC CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCC CS ast = binarc; 
#ifdef DEBUG
	  printfx ("result = %d\n", CCC CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %d, %c\n", CCC CS ast, CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 769:
	  binarcs = CCSC CS ast + CCSC CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCSC CS ast = binarcs; 
#ifdef DEBUG
	  printfx ("result = %d\n", CCSC CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %d, %c\n", CCSC CS ast, CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] + [[AST-1]] */
	case 770:
	  binarcu = CCUC CS ast + CCUC CS (ast + sizeof (char *));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCUC CS ast = binarcu;
#ifdef DEBUG
	  printfx ("result = %u\n", CCUC CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "ADD result %u, %c\n", CCUC CS ast, CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;
    case SUB:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST-1]] - [[AST]] */
	case 0:
	  binar = CCI CS (ast + sizeof (char *)) - CCI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef DEBUG
	  printfx ("SUB in ast\n");
	  printfx ("value = %d\n", CCI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "SUB result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* STACK <- STACK-NUM */
	case 1:
	  stack -= ((struct OPERAND_1_i *)pc1)->num;
	  check_stack ();
#ifdef DEBUG
	  printfx ("SUB of stack\n");
	  printfx ("value = %u\n", stack);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "SUB of stack, result %u\n", stack);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* [[AST-1]] <- [[AST-1]] - [[AST]] */
	case 2:
	  binaru = CCUI CS (ast + sizeof (char *)) - CCUI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CS ast = tmp;
	  CCUI CS ast = binaru;
#ifdef DEBUG
	  printfx ("SUB in ast\n");
	  printfx ("value = %u\n", CCUI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "SUB result %u\n", CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] - [[AST]] */
	case 3:
	  binarl = CCLI CS (ast + sizeof (char *)) - CCLI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CS ast = tmp;
	  CCLI CS ast = binarl;
#ifdef DEBUG
	  printfx ("SUB in ast\n");
	  printfx ("value = %ld\n", CCLI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "SUB result %ld\n", CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] - [[AST]] */
	case 4:
	  binarlu = CCLUI CS (ast + sizeof (char *)) - CCLUI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CS ast = tmp;
	  CCLUI CS ast = binarlu;
#ifdef DEBUG
	  printfx ("SUB in ast\n");
	  printfx ("value = %lu\n", CCLUI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "SUB result %lu\n", CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] - [[AST]] */
	case 256:
	  binard = CCD CS (ast + sizeof (char *)) - CCD CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (double));
	  CS ast = tmp;
	  CCD CS ast = binard;
#ifdef DEBUG
	  printfx ("SUB in ast\n");
	  printfx ("value = %lf\n", CCD CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "SUB result %lf\n", CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] - [[AST]] */
	case 257:
	  binarld = CCLD CS (ast + sizeof (char *)) - CCLD CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CS ast = tmp;
	  CCLD CS ast = binarld;
#ifdef DEBUG
	  printfx ("SUB in ast\n");
	  printfx ("value = %lf\n", CCLD CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "SUB result %lf\n", CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] - [[AST]] */
	case 512:
	  binarf = CCF CS (ast + sizeof (char *)) - CCF CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (float));
	  CS ast = tmp;
	  CCF CS ast = binarf;
#ifdef DEBUG
	  printfx ("SUB in ast\n");
	  printfx ("value = %g\n", CCF CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "SUB result %g\n", CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST-1]] - [[AST]] */
	case 768:
	  binarc = CCC CS (ast + sizeof (char *)) - CCC CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCC CS ast = binarc;
#ifdef DEBUG
	  printfx ("SUB in ast\n");
	  printfx ("value = %d\n", CCC CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "SUB result %d, %c\n", CCC CS ast,
		    CCC CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break; 

    case MULT:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST]] * [[AST-1]] */
	case 0:
	  binar = CCI CS ast * (CCI CS (ast + sizeof (char *)));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MULT result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] * [[AST-1]] */
	case 1:
	  binaru = CCUI CS ast * (CCUI CS (ast + sizeof (char *)));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CS ast = tmp;
	  CCUI CS ast = binaru;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MULT result %u\n", CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] * [[AST-1]] */
	case 2:
	  binarl = CCLI CS ast * (CCLI CS (ast + sizeof (char *)));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CS ast = tmp;
	  CCLI CS ast = binarl;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MULT result %ld\n", CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] * [[AST-1]] */
	case 3:
	  binarlu = CCLUI CS ast * (CCLUI CS (ast + sizeof (char *)));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CS ast = tmp;
	  CCLUI CS ast = binarlu;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MULT result %lu\n", CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] * [[AST-1]] */
	case 256:
	  binard = CCD CS ast * (CCD CS (ast + sizeof (char *)));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (double));
	  CS ast = tmp;
	  CCD CS ast = binard;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MULT result %lf\n", CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] * [[AST-1]] */
	case 257:
	  binarld = CCLD CS ast * (CCLD CS (ast + sizeof (char *)));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CS ast = tmp;
	  CCLD CS ast = binard;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MULT result %lf\n", CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST]] * [[AST-1]] */
	case 512:
	  binarf = CCF CS ast * (CCF CS (ast + sizeof (char *)));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (float));
	  CS ast = tmp;
	  CCF CS ast = binarf;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MULT result %g\n", CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST]] * [[AST-1]] */
	case 768:
	  binarc = CCC CS ast * (CCC CS (ast + sizeof (char *)));
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCC CS ast = binarc;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "MULT result %d, %c\n",
		    CCC CS ast, CCC CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case MOD:
      if (CCI CS ast != 0)
	mod_yes ();
      else
	{
	  fprintfx (stderr, "\ninterpreter: divide by zero \n\n");
	  return (-2);
	}
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "MOD result %d\n", CCI CS ast);
#endif
      pc += sizeof (struct OPERAND_0_ma);
      break;

    case DIV:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST-1]] / [[AST]] */
	case 0:
	  if (CCI CS ast != 0)
	    div_yes ();
	  else
	    {
	      fprintfx (stderr, "\ninterpreter: divide by zero \n\n");
	      return (-2);
	    } 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop,"DIV result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] / [[AST]] */
	case 1:
	  if (CCUI CS ast != 0)
	    divui_yes ();
	  else
	    {
	      fprintfx (stderr, "\ninterpreter: divide by zero \n\n");
	      return (-2);
	    } 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop,"DIV result %u\n", CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] / [[AST]] */
	case 2:
	  if (CCUI CS ast != 0)
	    divli_yes ();
	  else
	    {
	      fprintfx (stderr, "\ninterpreter: divide by zero \n\n");
	      return (-2);
	    } 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop,"DIV result %ld\n", CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] / [[AST]] */
	case 3:
	  if (CCLUI CS ast != 0)
	    divlui_yes ();
	  else
	    {
	      fprintfx (stderr, "\ninterpreter: divide by zero \n\n");
	      return (-2);
	    } 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop,"DIV result %lu\n", CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] / [[AST]] */
	case 256:
	  if (CCD CS ast != 0)
	    divd_yes ();
	  else
	    {
	      fprintfx (stderr, "\ninterpreter: divide by zero \n\n");
	      return (-2);
	    } 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "DIV result %lf\n", CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] / [[AST]] */
	case 257:
	  if (CCLD CS ast != 0)
	    divld_yes ();
	  else
	    {
	      fprintfx (stderr, "\ninterpreter: divide by zero \n\n");
	      return (-2);
	    } 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "DIV result %lf\n", CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] / [[AST]] */
	case 512:
	  if (CCF CS ast != 0)
	    divf_yes ();
	  else
	    {
	      fprintfx (stderr, "\ninterpreter: divide by zero \n\n");
	      return (-2);
	    }
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "DIV result %g\n", CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST-1]] / [[AST]] */
	case 768:
	  if (CCC CS ast != 0)
	    divc_yes ();
	  else
	    {
	      fprintfx (stderr, "\ninterpreter: divide by zero \n\n");
	      return (-2);
	    }
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "DIV result %d, %c\n", CCC CS ast, 
		    CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case OR:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST-1]] OR [[AST]] */
	case 0:
	  binar = CCI CS (ast + sizeof (char *)) || CCI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OR result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] OR [[AST]] */
	case 1:
	  binar = CCUI CS (ast + sizeof (char *)) || CCUI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OR result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] OR [[AST]] */
	case 2:
	  binar = CCLI CS (ast + sizeof (char *)) || CCLI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OR result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] OR [[AST]] */
	case 3:
	  binar = CCLUI CS (ast + sizeof (char *)) || CCLUI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OR result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] OR [[AST]] */
	case 256: binar = CCD CS (ast + sizeof (char *)) || CCD CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OR result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] OR [[AST]] */
	case 257: binar = CCLD CS (ast + sizeof (char *)) || CCLD CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OR result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] OR [[AST]] */
	case 512:
	  binar = CCF CS (ast + sizeof (char *)) || CCF CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OR result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST-1]] OR [[AST]] */
	case 768:
	  binar = CCC CS (ast + sizeof (char *)) || CCC CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OR result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case AND:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST-1]] AND [[AST]] */
	case 0:
	  binar = CCI CS (ast + sizeof (char *)) && CCI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "AND result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] AND [[AST]] */
	case 1:
	  binar = CCUI CS (ast + sizeof (char *)) && CCUI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "AND result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] AND [[AST]] */
	case 2:
	  binar = CCLI CS (ast + sizeof (char *)) && CCLI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "AND result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] AND [[AST]] */
	case 3:
	  binar = CCLUI CS (ast + sizeof (char *)) && CCLUI CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "AND result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] AND [[AST]] */
	case 256:
	  binar = CCD CS (ast + sizeof (char *)) && CCD CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "AND result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] AND [[AST]] */
	case 257:
	  binar = CCLD CS (ast + sizeof (char *)) && CCLD CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "AND result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] AND [[AST]] */
	case 512:
	  binar = CCF CS (ast + sizeof (char *)) && CCF CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "AND result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST-1]] AND [[AST]] */
	case 768:
	  binar = CCC CS (ast + sizeof (char *)) && CCC CS ast;
	  ast += sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = binar;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "AND result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case ORB:
      binar = CCI CS (ast + sizeof (char *)) | CCI CS ast;
      ast += sizeof (char *);
      move_stack_aligned (&tmp, -sizeof (int));
      CS ast = tmp;
      CCI CS ast = binar;
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "ORB result %d\n", CCI CS ast);
#endif
      pc += sizeof (struct OPERAND_0_ma);
      break;

    case ANDB:
      binar = CCI CS (ast + sizeof (char *)) & CCI CS ast;
      ast += sizeof (char *);
      move_stack_aligned (&tmp, -sizeof (int));
      CS ast = tmp;
      CCI CS ast = binar;
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "ANDB result %d\n", CCI CS ast);
#endif
      pc += sizeof (struct OPERAND_0_ma);
      break;

    case EQ:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST-1]] EQ [[AST]] */
	case 0:
	  if (CCI CS (ast + sizeof (char *)) == CCI CS ast)
	    vtrue ();
	  else 
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "EQ result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] EQ [[AST]] */
	case 1:
	  if (CCUI CS (ast + sizeof (char *)) == CCUI CS ast)
	    vtrue ();
	  else 
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "EQ result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] EQ [[AST]] */
	case 2:
	  if (CCLI CS (ast + sizeof (char *)) == CCLI CS ast)
	    vtrue ();
	  else 
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "EQ result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] EQ [[AST]] */
	case 3:
	  if (CCLUI CS (ast + sizeof (char *)) == CCLUI CS ast)
	    vtrue ();
	  else 
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "EQ result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] EQ [[AST]] */
	case 256:
	  if (CCD CS (ast + sizeof (char *)) == CCD CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "EQ result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] EQ [[AST]] */
	case 257:
	  if (CCLD CS (ast + sizeof (char *)) == CCLD CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "EQ result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] EQ [[AST]] */
	case 512:
	  if (CCF CS (ast + sizeof (char *)) == CCF CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "EQ result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST-1]] EQ [[AST]] */
	case 768:
	  if (CCC CS (ast + sizeof (char *)) == CCC CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "EQ result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case GR:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST-1]] GR [[AST]] */
	case 0:
	  if (CCI CS (ast + sizeof (char *)) > CCI CS ast)
	    vtrue ();
	  else
	    vfalse (); 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GR result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GR [[AST]] */
	case 1:
	  if (CCUI CS (ast + sizeof (char *)) > CCUI CS ast)
	    vtrue ();
	  else
	    vfalse (); 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GR result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GR [[AST]] */
	case 2:
	  if (CCLI CS (ast + sizeof (char *)) > CCLI CS ast)
	    vtrue ();
	  else
	    vfalse (); 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GR result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GR [[AST]] */
	case 3:
	  if (CCLUI CS (ast + sizeof (char *)) > CCLUI CS ast)
	    vtrue ();
	  else
	    vfalse (); 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GR result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GR [[AST]] */
	case 256:
	  if (CCD CS (ast + sizeof (char *)) > CCD CS ast)
	    vtrue ();
	  else
	    vfalse (); 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GR result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GR [[AST]] */
	case 257:
	  if (CCLD CS (ast + sizeof (char *)) > CCLD CS ast)
	    vtrue ();
	  else
	    vfalse (); 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GR result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GR [[AST]] */
	case 512:
	  if (CCF CS (ast + sizeof (char *)) > CCF CS ast)
	    vtrue ();
	  else
	    vfalse (); 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GR result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST-1]] GR [[AST]] */
	case 768:
	  if (CCC CS (ast + sizeof (char *)) > CCC CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GR result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case LO:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST-1]] LO [[AST]] */
	case 0:
	  if (CCI CS (ast + sizeof (char *)) < CCI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LO result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LO [[AST]] */
	case 1:
	  if (CCUI CS (ast + sizeof (char *)) < CCUI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LO result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LO [[AST]] */
	case 2:
	  if (CCLI CS (ast + sizeof (char *)) < CCLI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LO result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LO [[AST]] */
	case 3:
	  if (CCLUI CS (ast + sizeof (char *)) < CCLUI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LO result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LO [[AST]] */
	case 256:
	  if (CCD CS (ast + sizeof (char *)) < CCD CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LO result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LO [[AST]] */
	case 512:
	  if (CCF CS (ast + sizeof (char *)) < CCF CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LO result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST-1]] LO [[AST]] */
	case 768:
	  if (CCC CS (ast + sizeof (char *)) < CCC CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LO result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case LE:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST-1]] LE [[AST]] */
	case 0:
	  if (CCI CS (ast + sizeof (char *)) <= CCI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LE [[AST]] */
	case 1:
	  if (CCUI CS (ast + sizeof (char *)) <= CCUI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LE [[AST]] */
	case 2:
	  if (CCLI CS (ast + sizeof (char *)) <= CCLI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LE [[AST]] */
	case 3:
	  if (CCLUI CS (ast + sizeof (char *)) <= CCLUI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LE [[AST]] */
	case 256:
	  if (CCD CS (ast + sizeof (char *)) <= CCD CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LE [[AST]] */
	case 257:
	  if (CCLD CS (ast + sizeof (char *)) <= CCLD CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] LE [[AST]] */
	case 512:
	  if (CCF CS (ast + sizeof (char *)) <= CCF CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST-1]] LE [[AST]] */
	case 768:
	  if (CCC CS (ast + sizeof (char *)) <= CCC CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "LE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case GE:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST-1]] GE [[AST]] */
	case 0:
	  if (CCI CS (ast + sizeof (char *)) >= CCI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GE [[AST]] */
	case 1:
	  if (CCUI CS (ast + sizeof (char *)) >= CCUI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GE [[AST]] */
	case 2:
	  if (CCLI CS (ast + sizeof (char *)) >= CCLI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GE [[AST]] */
	case 3:
	  if (CCLUI CS (ast + sizeof (char *)) >= CCLUI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GE [[AST]] */
	case 256:
	  if (CCD CS (ast + sizeof (char *)) >= CCD CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GE [[AST]] */
	case 257:
	  if (CCLD CS (ast + sizeof (char *)) >= CCLD CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] GE [[AST]] */
	case 512:
	  if (CCF CS (ast + sizeof (char *)) >= CCF CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST-1]] GE [[AST]] */
	case 768:
	  if (CCC CS (ast + sizeof (char *)) >= CCC CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "GE result %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case NE:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST-1]] <- [[AST-1]] NE [[AST]] */
	case 0:
	  if (CCI CS (ast + sizeof (char *)) != CCI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NE result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] NE [[AST]] */
	case 1:
	  if (CCUI CS (ast + sizeof (char *)) != CCUI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NE result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] NE [[AST]] */
	case 2:
	  if (CCLI CS (ast + sizeof (char *)) != CCLI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NE result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] NE [[AST]] */
	case 3:
	  if (CCLUI CS (ast + sizeof (char *)) != CCLUI CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NE result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] NE [[AST]] */
	case 256:
	  if (CCD CS (ast + sizeof (char *)) != CCD CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NE result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] NE [[AST]] */
	case 257:
	  if (CCLD CS (ast + sizeof (char *)) != CCLD CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NE result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST-1]] <- [[AST-1]] NE [[AST]] */
	case 512:
	  if (CCF CS (ast + sizeof (char *)) != CCF CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NE result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST-1]] <- [[AST-1]] NE [[AST]] */
	case 768:
	  if (CCC CS (ast + sizeof (char *)) != CCC CS ast)
	    vtrue ();
	  else
	    vfalse ();
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NE result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case NEG:
      switch (((struct OPERAND_0_mi *)pc1)->minor)
	{
	  /* [[AST]] <- NEG [[AST]] */
	case 0:
	  if ( ! CCI CS ast) 
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vtrue ();
	    }
	  else 
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vfalse ();
	    }
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NEG result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- NEG [[AST]] */
	case 1:
	  if ( ! CCUI CS ast) 
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vtrue ();
	    }
	  else 
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vfalse ();
	    }
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NEG result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- NEG [[AST]] */
	case 2:
	  if ( ! CCLI CS ast) 
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vtrue ();
	    }
	  else 
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vfalse ();
	    }
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NEG result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- NEG [[AST]] */
	case 3:
	  if ( ! CCLUI CS ast) 
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vtrue ();
	    }
	  else 
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vfalse ();
	    }
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NEG result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- NEG [[AST]] */
	case 256:
	  if ( ! CCD CS ast)
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vtrue ();
	    }
	  else
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vfalse ();
	    }
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NEG result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- NEG [[AST]] */
	case 257:
	  if ( ! CCLD CS ast)
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vtrue ();
	    }
	  else
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vfalse ();
	    }
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NEG result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

	  /* [[AST]] <- NEG [[AST]] */
	case 512:
	  if ( ! CCF CS ast)
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vtrue ();
	    }
	  else
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vfalse ();
	    }
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NEG result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;

#if 0
	  /* [[AST]] <- NEG [[AST]] */
	case 768:
	  if ( ! CCC CS ast)
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vtrue ();
	    }
	  else
	    {
	      /* Hack for using vtrue () and vfalse (). */
	      ast -= sizeof (char *);
	      vfalse ();
	    }
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "NEG result %d\n", CCI CS ast); 
#endif
	  pc += sizeof (struct OPERAND_0_mi);
	  break;
#endif

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case NOT:
      move_stack_aligned (&tmp, -sizeof (int));
      CCI tmp= ~(CCI CS ast);
      CS ast = tmp;
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "NOT result %d\n", CCI CS ast); 
#endif
      pc += sizeof (struct OPERAND_0_ma);
      break;

    case SAL:
      binar = CCI CS (ast + sizeof (char *)) << CCI CS ast;
      ast += sizeof (char *);
      move_stack_aligned (&tmp, -sizeof (int));
      CS ast = tmp;
      CCI CS ast = binar;
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "SAL result %d\n", CCI CS ast);
#endif
      pc += sizeof (struct OPERAND_0_ma);
      break;

    case SAR:
      binar = CCI CS (ast + sizeof (char *)) >> CCI CS ast;
      ast += sizeof (char *);
      move_stack_aligned (&tmp, -sizeof (int));
      CS ast = tmp;
      CCI CS ast = binar;
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "SAR result %d\n", CCI CS ast);
#endif
      pc += sizeof (struct OPERAND_0_ma);
      break;

    case XOR:
      binar = CCI CS (ast + sizeof (char *)) ^ CCI CS ast;
      ast += sizeof (char *);
      move_stack_aligned (&tmp, -sizeof (int));
      CS ast = tmp;
      CCI CS ast = binar;
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "XOR result %d\n", CCI CS ast);
#endif
      pc += sizeof (struct OPERAND_0_ma);
      break;

    case STOP:
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "STOP\n");
#endif
      pc += sizeof (struct OPERAND_0_ma);
      return (-1);
      break;

    case INTER:
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "INTER\n");
#endif
#ifndef MSDOS
      interrupt_service_sync ();
#endif
      pc += sizeof (struct OPERAND_0_ma);
      break;

    case IRET:
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "IRET\n");
#endif
      pc += sizeof (struct OPERAND_0_ma);
#ifndef NOT_MSWIN_AND_YES_DOS
      restore_context ();
      fprintfx (stderr, "\nclif interrupt level %d\n",
		CLIF_INTERRUPT_LEVEL_DECREMENT);
#endif
      break;

    case PUSHAI:
      switch (((struct OPERAND_1_i *)pc1)->minor)
	{
	  /* [AST] <- NUM */
	case 0:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast = (((struct OPERAND_1_i *)pc1)->num);
#ifdef DEBUG
	  printfx ("saved number in ast:%u\n", CCI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHAI to address %u value %d\n",
		    CS ast, CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* [AST] <- NUM */
	case 1:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CS ast = tmp;
	  CCUI CS ast = (((struct OPERAND_1_ui *)pc1)->num);
#ifdef DEBUG
	  printfx ("saved number in ast:%u\n", CCUI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHAI to address %u value %u\n",
		    CS ast, CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_ui);
	  break;

	  /* [AST] <- NUM */
	case 2:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CS ast = tmp;
	  CCLI CS ast = (((struct OPERAND_1_li *)pc1)->num);
#ifdef DEBUG
	  printfx ("saved number in ast:%ld\n", CCLI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHAI to address %u value %ld\n",
		    CS ast, CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_li);
	  break;

	  /* [AST] <- NUM */
	case 3:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CS ast = tmp;
	  CCLUI CS ast = (((struct OPERAND_1_lui *)pc1)->num);
#ifdef DEBUG
	  printfx ("saved number in ast:%lu\n", CCLUI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHAI to address %u value %lu\n",
		    CS ast, CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_lui);
	  break;

	  /* [AST] <- NUM */
	case 4:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CS ast = tmp;
	  CCSI CS ast = (((struct OPERAND_1_si *)pc1)->num);
#ifdef DEBUG
	  printfx ("saved number in ast:%hd\n", CCSI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHAI to address %u value %hd\n",
		    CS ast, CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_si);
	  align_memory (&pc, sizeof (int));
	  break;

	  /* [AST] <- NUM */
	case 5:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CS ast = tmp;
	  CCSUI CS ast = (((struct OPERAND_1_sui *)pc1)->num);
#ifdef DEBUG
	  printfx ("saved number in ast:%hu\n", CCSUI CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHAI to address %u value %hu\n",
		    CS ast, CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_sui);
	  align_memory (&pc, sizeof (int));
	  break;

	  /* [AST] <- NUM */
	case 256:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (double));
	  CS ast = tmp;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
#endif
	  pc1 += sizeof (struct OPERAND_0_mi);
	  align_memory (&pc1, sizeof (double));
	  CCD CS ast = *(double *)pc1;
#ifdef DEBUG
	  printfx ("saved number in ast:%lf\n", CCD CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "PUSHAI to address %u value %lf\n",
		    CS ast, CCD CS ast);
#endif
	  pc1 += sizeof (double);
	  pc = pc1;
	  break;

	  /* [AST] <- NUM */
	case 257:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CS ast = tmp;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
#endif
	  pc1 += sizeof (struct OPERAND_0_mi);
	  align_memory (&pc1, sizeof (long double));
	  CCLD CS ast = *(long double *)pc1;
#ifdef DEBUG
	  printfx ("saved number in ast:%lf\n", CCLD CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "PUSHAI to address %u value %lf\n",
		    CS ast, CCLD CS ast);
#endif
	  pc1 += sizeof (long double);
	  pc = pc1;
	  break;
	
	  /* [AST] <- NUM */
	case 512:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (float));
	  CS ast = tmp;
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
#endif
	  pc1 += sizeof (struct OPERAND_0_mi);
	  align_memory (&pc1, sizeof (float));
	  CCF CS ast = *(float *)pc1;
#ifdef DEBUG
	  printfx ("saved number in ast:%g\n", CCF CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "PUSHAI to address %u value %g\n",
		    CS ast, CCF CS ast);
#endif
	  pc1 += sizeof (float);
	  pc = pc1;
	  break;

	  /* [AST] <- NUM */
	case 768:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCC CS ast = (((struct OPERAND_1_ic *)pc1)->num);
#ifdef DEBUG
	  printfx ("saved number in ast:%c\n", CCC CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHAI to address %u value %c\n",
		    CS ast, CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_ic);
	  align_memory (&pc, sizeof (int));
	  break;

	  /* [AST] <- STRING */
	case 769:
	  ast -= sizeof (char *);
	  binar = strlen (((struct OPERAND_1_mi *)pc1)->adr) + 1;
#if 0
	  if (binar & MASK)
	    tmp -= (4 - (binar & MASK) + binar);
	  else
#endif
	    tmp -= binar;
	  CS ast = tmp;
	  strcpy (CS ast, (((struct OPERAND_1_mi *)pc1)->adr));
#ifdef DEBUG
	  printfx ("saved number in ast:%s\n", CCC CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHAI to address %u value %s\n", CS ast, *ast);
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* [AST] <- NUM */
	case 770:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCSC CS ast = (((struct OPERAND_1_isc *)pc1)->num);
#ifdef DEBUG
	  printfx ("saved number in ast:%c\n", CCSC CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHAI to address %u value %c\n",
		    CS ast, CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_isc);
	  align_memory (&pc, sizeof (int));
	  break;

	  /* [AST] <- NUM */
	case 771:
	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCUC CS ast = (((struct OPERAND_1_iuc *)pc1)->num);
#ifdef DEBUG
	  printfx ("saved number in ast:%c\n", CCUC CS ast);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "PUSHAI to address %u value %c\n",
		    CS ast, CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_iuc);
	  align_memory (&pc, sizeof (int));
	  break;

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case OUT:
      switch (((struct OPERAND_1_mi *)pc1)->minor)
	{
	  /* OUT [ADR] */
	case 0:
	  printfx ("%d\n", CCI (((struct OPERAND_1_mi *)pc1)->adr));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d\n",
		    CCI (((struct OPERAND_1_mi *)pc1)->adr)); 
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* OUT [BP+NUM] */
	case 1:
	  printfx ("%d\n", CCI (((struct OPERAND_1_i *)pc1)->num + bp));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d\n",
		    CCI (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [[BP+NUM]] */
	case 2:
	  printfx ("%d\n", CCI CS (((struct OPERAND_1_i *)pc1)->num + bp));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d\n",
		    CCI CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [ADR+INDEX] */
	case 3:
	  printfx ("%d\n", CCI ((((struct OPERAND_1_mi *)pc1)->adr) +
				CCI CS ast * sizeof (int)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d\n",
		    CCI ((((struct OPERAND_1_mi *)pc1)->adr) +
			 CCI CS ast * sizeof (int)));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* OUT [BP+NUM+INDEX] */
	case 4:
	  printfx ("%d\n", CCI (((struct OPERAND_1_i *)pc1)->num + bp
				+ CCI CS ast * sizeof (int)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d\n",
		    CCI (((struct OPERAND_1_i *)pc1)->num + bp + 
			 CCI CS ast * sizeof (int)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [[BP+NUM+INDEX]] */
	case 5:
	  printfx ("%d\n", CCI (CS (((struct OPERAND_1_i *)pc1)->num +
				    bp)+ CCI CS ast * sizeof (int)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d\n",
		    CCI (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			 CCI CS ast * sizeof (int)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [ADR] */
	case 256:
	  printfx ("%lg\n", CCD (((struct OPERAND_1_mi *)pc1)->adr));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop,"OUT %lg\n",
		    CCD (((struct OPERAND_1_mi *)pc1)->adr));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* OUT [BP+NUM] */
	case 257:
	  printfx ("%lg\n",
		   CCD (((struct OPERAND_1_i *)pc1)->num + bp));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %lg\n", 
		    CCD (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [[BP+NUM]] */
	case 258:
	  printfx ("%lg\n",
		   CCD CS (((struct OPERAND_1_i *)pc1)->num + bp));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %lg\n",
		    CCD CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [ADR+INDEX] */
	case 259:
	  printfx ("%lg\n", 
		   CCD ((((struct OPERAND_1_mi *)pc1)->adr) + CCI CS
			ast * sizeof (double)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %lg\n", 
		    CCD ((((struct OPERAND_1_mi *)pc1)->adr) + CCI CS
			 ast * sizeof (double)));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* OUT [BP+NUM+INDEX] */
	case 260:
	  printfx ("%lg\n", 
		   CCD (((struct OPERAND_1_i *)pc1)->num + bp + CCI CS
			ast * sizeof (double)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %lg\n",
		    CCD (((struct OPERAND_1_i *)pc1)->num + bp + CCI
			 CS ast * sizeof (double)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;
	
	  /* OUT [[BP+NUM+INDEX]] */
	case 261:
	  printfx ("%lg\n",
		   CCD (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			CCI CS ast * sizeof (double)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %lg\n", 
		    CCD (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			 CCI CS ast * sizeof (double)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [ADR] */
	case 512:
	  printfx ("%g\n", CCF (((struct OPERAND_1_mi *)pc1)->adr));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %g\n",
		    CCF (((struct OPERAND_1_mi *)pc1)->adr));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* OUT [BP+NUM] */
	case 513:
	  printfx ("%g\n", CCF (((struct OPERAND_1_i *)pc1)->num + bp));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %g\n",
		    CCF (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [[BP+NUM]] */
	case 514:
	  printfx ("%g\n",
		   CCF CS (((struct OPERAND_1_i *)pc1)->num + bp)); 
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %g\n",
		    CCF CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [ADR+INDEX] */
	case 515:
	  printfx ("%g\n",
		   CCF ((((struct OPERAND_1_mi *)pc1)->adr) +
			CCI CS ast * sizeof (float)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %g\n",
		    CCF ((((struct OPERAND_1_mi *)pc1)->adr) +
			 CCI CS ast * sizeof (float)));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* OUT [BP+NUM+INDEX] */
	case 516:
	  printfx ("%g\n",
		   CCF (((struct OPERAND_1_i *)pc1)->num + bp +
			CCI CS ast * sizeof (float)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %g\n",
		    CCF (((struct OPERAND_1_i *)pc1)->num + bp + 
			 CCI CS ast * sizeof (float)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [[BP+NUM+INDEX]] */
	case 517:
	  printfx ("%g\n",
		   CCF (CS (((struct OPERAND_1_i *)pc1)->num + bp) + 
			CCI CS ast * sizeof (float)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %g\n",
		    CCF (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			 CCI CS ast * sizeof (float)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [ADR] */
	case 768:
	  printfx ("%d\n", CCC (((struct OPERAND_1_mi *)pc1)->adr));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d, %c\n",
		    CCC (((struct OPERAND_1_mi *)pc1)->adr),
		    CCC (((struct OPERAND_1_mi *)pc1)->adr));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* OUT [BP+NUM] */
	case 769:
	  printfx ("%d\n", CCC (((struct OPERAND_1_i *)pc1)->num + bp));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d, %c\n",
		    CCC (((struct OPERAND_1_i *)pc1)->num + bp),
		    CCC (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [[BP+NUM]] */
	case 770:
	  printfx ("%d\n",
		   CCC CS (((struct OPERAND_1_i *)pc1)->num + bp));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d, %c\n",
		    CCC CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [ADR+INDEX] */
	case 771:
	  printfx ("%d\n",
		   CCC ((((struct OPERAND_1_mi *)pc1)->adr) +
			CCI CS ast * sizeof (char)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d, %c\n",
		    CCC ((((struct OPERAND_1_mi *)pc1)->adr) +
			 CCI CS ast * sizeof (char)),
		    CCC ((((struct OPERAND_1_mi *)pc1)->adr) +
			 CCI CS ast * sizeof (char)));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* OUT [BP+NUM+INDEX] */
	case 772:
	  printfx ("%d\n",
		   CCC (((struct OPERAND_1_i *)pc1)->num + bp +
			CCI CS ast * sizeof (char)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d, %c\n",
		    CCC (((struct OPERAND_1_i *)pc1)->num + bp +
			 CCI CS ast * sizeof (char)),
		    CCC (((struct OPERAND_1_i *)pc1)->num + bp +
			 CCI CS ast * sizeof (char)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* OUT [[BP+NUM+INDEX]] */
	case 773:
	  printfx ("%d\n",
		   CCC (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			CCI CS ast * sizeof (char)));
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "OUT %d, %c\n",
		    CCC (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			 CCI CS ast * sizeof (char)),
		    CCC (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			 CCI CS ast * sizeof (char)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case IN:
      switch (((struct OPERAND_1_mi *)pc1)->minor)
	{
	  /* IN [ADR] */
	case 0:
	  scanf ("%d", (int *)(((struct OPERAND_1_mi *)pc1)->adr));
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCI (((struct OPERAND_1_mi *)pc1)->adr));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d\n", 
		    CCI (((struct OPERAND_1_mi *)pc1)->adr));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* IN [BP+NUM] */
	case 1:
	  scanf ("%d",
		 (int *)(((struct OPERAND_1_i *)pc1)->num + bp)); 
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCI (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d\n",
		    CCI (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [[BP+NUM]] */
	case 2:
	  scanf ("%d",
		 ((int *) CS (((struct OPERAND_1_i *)pc1)->num + bp)));
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCI CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d\n",
		    CCI CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [ADR+INDEX] */
	case 3:
	  scanf("%d",
		(int *)((((struct OPERAND_1_mi *)pc1)->adr) +
			CCI CS ast * sizeof (int)));
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCI ((((struct OPERAND_1_mi *)pc1)->adr) +
			CCI CS ast * sizeof (int)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d\n",
		    CCI ((((struct OPERAND_1_mi *)pc1)->adr) +
			 CCI CS ast * sizeof (int)));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* IN [BP+NUM+INDEX] */
	case 4:
	  scanf ("%d",
		 (int *)(((struct OPERAND_1_i *)pc1)->num + bp +
			 CCI CS ast * sizeof (int)));
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCI (((struct OPERAND_1_i *)pc1)->num + bp +
			CCI CS ast * sizeof (int)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d\n",
		    CCI (((struct OPERAND_1_i *)pc1)->num + bp +
			 CCI CS ast * sizeof (int)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [[BP+NUM+INDEX]] */
	case 5:
	  scanf("%d",
		((int *) (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			  CCI CS ast * sizeof (int))));
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCI (CS (((struct OPERAND_1_i *)pc1)->num + bp) + 
			CCI CS ast * sizeof (int)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d\n",
		    CCI (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			 CCI CS ast * sizeof (int)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [ADR] */
	case 256:
	  scanf("%lf", (double *)(((struct OPERAND_1_mi *)pc1)->adr));
#ifdef DEBUG
	  printfx ("saved :%lf\n",
		   CCD (((struct OPERAND_1_mi *)pc1)->adr));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %lf\n",
		    CCD (((struct OPERAND_1_mi *)pc1)->adr));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* IN [BP+NUM] */
	case 257:
	  scanf ("%lf",
		 (double *)(((struct OPERAND_1_i *)pc1)->num + bp));
#ifdef DEBUG
	  printfx ("saved :%lf\n",
		   CCD (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %lf\n",
		    CCD (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [[BP+NUM]] */
	case 258:
	  scanf ("%lf",
		 ((double *) CS (((struct OPERAND_1_i *)pc1)->num + bp)));
#ifdef DEBUG
	  printfx ("saved :%lf\n",
		   CCD CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %lf\n",
		    CCD CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [ADR+INDEX] */
	case 259:
	  scanf ("%lf",
		 (double *)((((struct OPERAND_1_mi *)pc1)->adr) +
			    CCI CS ast * sizeof (double)));
#ifdef DEBUG
	  printfx ("saved :%lf\n",
		   CCD ((((struct OPERAND_1_mi *)pc1)->adr) +
			CCI CS ast * sizeof (double)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %lf\n",
		    CCD ((((struct OPERAND_1_mi *)pc1)->adr) +
			 CCI CS ast * sizeof (double)));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* IN [BP+NUM+INDEX] */
	case 260:
	  scanf ("%lf",
		 (double *)(((struct OPERAND_1_i *)pc1)->num + bp +
			    CCI CS ast * sizeof (double)));
#ifdef DEBUG
	  printfx ("saved :%lf\n",
		   CCD (((struct OPERAND_1_i *)pc1)->num + bp +
			CCI CS ast * sizeof (double)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %lf\n",
		    CCD (((struct OPERAND_1_i *)pc1)->num + bp +
			 CCI CS ast * sizeof (double)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [[BP+NUM+INDEX]] */
	case 261:
	  scanf ("%lf",
		 ((double *) (CS (((struct OPERAND_1_i *)pc1)->num + bp)
			      + CCI CS ast * sizeof (double))));
#ifdef DEBUG
	  printfx ("saved :%lf\n",
		   CCD (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			CCI CS ast * sizeof (double)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %lf\n",
		    CCD (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			 CCI CS ast * sizeof (double)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [ADR] */
	case 512:
	  scanf("%f", (float *)(((struct OPERAND_1_mi *)pc1)->adr));
#ifdef DEBUG
	  printfx ("saved :%g\n",
		   CCF (((struct OPERAND_1_mi *)pc1)->adr));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %g\n",
		    CCF (((struct OPERAND_1_mi *)pc1)->adr));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* IN [BP+NUM] */
	case 513:
	  scanf ("%f",
		 (float *)(((struct OPERAND_1_i *)pc1)->num + bp));
#ifdef DEBUG
	  printfx ("saved :%f\n",
		   CCF (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %g\n",
		    CCF (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [[BP+NUM]] */
	case 514:
	  scanf ("%f",
		 ((float *) CS (((struct OPERAND_1_i *)pc1)->num + bp))); 
#ifdef DEBUG
	  printfx ("saved :%g\n",
		   CCF CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %g\n",
		    CCF CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [ADR+INDEX] */
	case 515:
	  scanf ("%f",
		 (float *)((((struct OPERAND_1_mi *)pc1)->adr) +
			   CCI CS ast * sizeof (float)));
#ifdef DEBUG
	  printfx ("saved :%g\n",
		   CCF ((((struct OPERAND_1_mi *)pc1)->adr) +
			CCI CS ast * sizeof (float)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %g\n",
		    CCF ((((struct OPERAND_1_mi *)pc1)->adr) +
			 CCI CS ast * sizeof (float)));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* IN [BP+NUM+INDEX] */
	case 516:
	  scanf ("%f",
		 (float *)(((struct OPERAND_1_i *)pc1)->num + bp +
			   CCI CS ast * sizeof (float)));
#ifdef DEBUG
	  printfx ("saved :%g\n",
		   CCF (((struct OPERAND_1_i *)pc1)->num + bp +
			CCI CS ast * sizeof (float)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %g\n",
		    CCF (((struct OPERAND_1_i *)pc1)->num + bp +
			 CCI CS ast * sizeof (float)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [[BP+NUM+INDEX]] */
	case 517:
	  scanf ("%f",
		 ((float *) (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			     CCI CS ast * sizeof (float))));
#ifdef DEBUG
	  printfx ("saved :%g\n",
		   CCF (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			CCI CS ast * sizeof (float)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %g\n",
		    CCF (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			 CCI CS ast * sizeof (float)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [ADR] */
	case 768:
	  scanf ("%1s", (char *)(((struct OPERAND_1_mi *)pc1)->adr));
#ifdef DEBUG
	  printfx ("saved :%d\n", CCC (((struct OPERAND_1_mi *)pc1)->adr));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d, %c\n",
		    CCC (((struct OPERAND_1_mi *)pc1)->adr),
		    CCC (((struct OPERAND_1_mi *)pc1)->adr));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* IN [BP+NUM] */
	case 769:
	  scanf ("%1s",
		 (char *)(((struct OPERAND_1_i *)pc1)->num + bp));
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCC (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d, %c\n",
		    CCC (((struct OPERAND_1_i *)pc1)->num + bp),
		    CCC (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [[BP+NUM]] */
	case 770:
	  scanf ("%1s",
		 ((char *) CS (((struct OPERAND_1_i *)pc1)->num + bp)));
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCC CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d, %c\n",
		    CCC CS (((struct OPERAND_1_i *)pc1)->num + bp),
		    CCC CS (((struct OPERAND_1_i *)pc1)->num + bp));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [ADR+INDEX] */
	case 771:
	  scanf ("%1s",
		 (char *)((((struct OPERAND_1_mi *)pc1)->adr) +
			  CCI CS ast * sizeof (char)));
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCC ((((struct OPERAND_1_mi *)pc1)->adr) +
			CCI CS ast * sizeof (char)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d, %c\n",
		    CCC ((((struct OPERAND_1_mi *)pc1)->adr) +
			 CCI CS ast * sizeof (char)),
		    CCC ((((struct OPERAND_1_mi *)pc1)->adr) +
			 CCI CS ast * sizeof (char)));
#endif
	  pc += sizeof (struct OPERAND_1_mi);
	  break;

	  /* IN [BP+NUM+INDEX] */
	case 772:
	  scanf ("%1s",
		 (char *)(((struct OPERAND_1_i *)pc1)->num + bp +
			  CCI CS ast * sizeof (char)));
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCC (((struct OPERAND_1_i *)pc1)->num + bp +
			CCI CS ast * sizeof (char)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d, %c\n",
		    CCC (((struct OPERAND_1_i *)pc1)->num + bp +
			 CCI CS ast * sizeof (char)),
		    CCC (((struct OPERAND_1_i *)pc1)->num + bp +
			 CCI CS ast * sizeof (char)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* IN [[BP+NUM+INDEX]] */
	case 773:
	  scanf ("%1s",
		 ((char *) (CS (((struct OPERAND_1_i *)pc1)->num + bp)
			    + CCI CS ast * sizeof (char))));
#ifdef DEBUG
	  printfx ("saved :%d\n",
		   CCC (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			CCI CS ast * sizeof (char)));
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "IN %d, %c\n",
		    CCC (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			 CCI CS ast * sizeof (char)),
		    CCC (CS (((struct OPERAND_1_i *)pc1)->num + bp) +
			 CCI CS ast * sizeof (char)));
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;

    case MESS:
      printfx ("%s\n", ((struct OPERAND_1_ma *)pc1)->adr);
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "MESS %s\n", ((struct OPERAND_1_ma *)pc1)->adr);
#endif
      pc += sizeof (struct OPERAND_1_ma);
      break;

    case JMP:
      pc = (((struct OPERAND_1_ma *)pc1)->adr);
#ifdef DEBUG
      printfx ("JMP jump to address=%u\n", pc); 
#endif
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "JMP to address %u\n", pc);
#endif
      break;

    case JZ:
      if (CCI CS ast == 0)
	{
	  pc = (((struct OPERAND_1_ma *)pc1)->adr);
#ifdef DEBUG
	  printfx ("JZ jump to address=%u\n", pc); 
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "JZ to address %u\n", pc); 
#endif
	}
      else
	pc += sizeof (struct OPERAND_1_ma);

      ast += sizeof (char *);
      break;

    case HALT:
#ifdef DEBUG
      printfx ("HALT\n");
#endif
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "HALT\n");
#endif
      pc += sizeof (struct OPERAND_0_ma);
      return (-2);
      break;

    case JNZ:
      if (CCI CS ast != 0)
	{
	  pc = (((struct OPERAND_1_ma *)pc1)->adr);
#ifdef DEBUG
	  printfx ("JNZ jump to address=%u\n", pc);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "JNZ to address %u\n", pc);
#endif
	}
      else
	pc += sizeof (struct OPERAND_1_ma);

      ast += sizeof (char *);
      break;

    case CALLi:
      switch (((struct OPERAND_1_mi *)pc1)->minor)
	{
	  /* CALL user function */
	case 0:
	  stack -= sizeof (char *);
	  check_stack ();
	  CS stack = (pc + sizeof (struct OPERAND_1_mi));
#ifdef DEBUG
	  printfx ("in STACK is value of pc=%u\n", CS stack);
#endif
	  pc = ((struct OPERAND_1_mi *)pc1)->adr;
#ifdef DEBUG
	  printfx ("new value of pc=%u\n", pc);
#endif
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "CALL %u\n", pc);
#endif
	  break;

	  /* CALL intrinsic integer function */
	case 1:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast =
	    (CFI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic double function */
	case 2:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (double));
	  CS ast = tmp;
	  CCD CS ast =
	    (CFD f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop,"\nreturn value = %g\n", CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic float function */
	case 3:
	  for (counter = 0; counter < (frame - stack) / 
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (float));
	  CS ast = tmp;
	  CCF CS ast =
	    (CFF f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop, "\nreturn value = %g\n", CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic char function */
	case 4:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCC CS ast =
	    (CFC f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop, "\nreturn value = %d\n", CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic void function */
	case 5:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  (CFV f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic unsigned integer function */
	case 6:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CS ast = tmp;
	  CCUI CS ast =
	    (CFUI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %u\n", CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic long integer function */
	case 7:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CS ast = tmp;
	  CCLI CS ast =
	    (CFLI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %ld\n", CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic long unsigned integer function */
	case 8:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CS ast = tmp;
	  CCLUI CS ast =
	    (CFLUI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %lu\n", CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic short integer function */
	case 9:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CS ast = tmp;
	  CCSI CS ast =
	    (CFSI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %hd\n", CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic short unsigned integer function */
	case 10:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CS ast = tmp;
	  CCSUI CS ast =
	    (CFSUI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %hu\n", CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic long double function */
	case 11:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CS ast = tmp;
	  CCLD CS ast =
	    (CFLD f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop,"\nreturn value = %g\n", CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic signed char function */
	case 12:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCSC CS ast =
	    (CFSC f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop, "\nreturn value = %d\n", CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic unsigned char function */
	case 13:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = CS (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCUC CS ast =
	    (CFUC f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop, "\nreturn value = %u\n", CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic integer function */
	case 14:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCI CS ast =
	    (CFI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %d\n", CCI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic double function */
	case 15:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (double));
	  CS ast = tmp;
	  CCD CS ast =
	    (CFD f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop,"\nreturn value = %g\n", CCD CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic float function */
	case 16:
	  for (counter = 0; counter < (frame - stack) / 
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (float));
	  CS ast = tmp;
	  CCF CS ast =
	    (CFF f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop, "\nreturn value = %g\n", CCF CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic char function */
	case 17:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCC CS ast =
	    (CFC f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop, "\nreturn value = %d\n", CCC CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic void function */
	case 18:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  (CFV f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic unsigned integer function */
	case 19:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (unsigned int));
	  CS ast = tmp;
	  CCUI CS ast =
	    (CFUI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %u\n", CCUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic long integer function */
	case 20:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long int));
	  CS ast = tmp;
	  CCLI CS ast =
	    (CFLI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %ld\n", CCLI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic long unsigned integer function */
	case 21:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long unsigned int));
	  CS ast = tmp;
	  CCLUI CS ast =
	    (CFLUI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %lu\n", CCLUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic short integer function */
	case 22:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (short int));
	  CS ast = tmp;
	  CCSI CS ast =
	    (CFSI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %hd\n", CCSI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic short unsigned integer function */
	case 23:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (short unsigned int));
	  CS ast = tmp;
	  CCSUI CS ast =
	    (CFSUI f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num); 
	  fprintfx (cop, "\nreturn value = %hu\n", CCSUI CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic long double function */
	case 24:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (long double));
	  CS ast = tmp;
	  CCLD CS ast =
	    (CFLD f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop,"\nreturn value = %g\n", CCLD CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic signed char function */
	case 25:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCSC CS ast =
	    (CFSC f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop, "\nreturn value = %d\n", CCSC CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	  /* CALL intrinsic unsigned char function */
	case 26:
	  for (counter = 0; counter < (frame - stack) /
		 sizeof (char *); counter++)
	    adr_arg[counter] = (stack + counter * sizeof (char *));

	  ast -= sizeof (char *);
	  move_stack_aligned (&tmp, -sizeof (int));
	  CS ast = tmp;
	  CCUC CS ast =
	    (CFUC f[((struct OPERAND_1_i *)pc)->num]) (adr_arg);
#ifdef CODE
	  fprintfx (cop, "\n%u   ", pc1);
	  fprintfx (cop, "XCALL %u\n",
		    ((struct OPERAND_1_i *)pc)->num);
	  fprintfx (cop, "\nreturn value = %u\n", CCUC CS ast);
#endif
	  pc += sizeof (struct OPERAND_1_i);
	  break;

	default:
	  fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
	  error_message (5001);
	  abort ();
	}
      break;
    case RET:
      pc = CS stack;
      stack += sizeof (char *);
#ifdef DEBUG
      printfx ("RET to value of pc=%u\n", pc);
#endif
#ifdef CODE
      fprintfx (cop, "\n%u   ", pc1);
      fprintfx (cop, "RET to %u\n", pc);
#endif 
      break;

    case XCHG:
    {
      char *tmp = CS ast;
      CS ast = CS (ast + sizeof (char *));
      CS (ast + sizeof (char *)) = tmp;
      pc += sizeof (struct OPERAND_0_ma);
      break;
    }

    default:
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);
      error_message (5001);
      abort ();
    }
  return 0;
}

/* 
 * Evaluation of logical expressions - true. 
 */ 
static void 
vtrue ()
{
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (int));
  CS ast = tmp;
  CCI CS ast = 1;
}

/* 
 * Evaluation of logical expressions - false.
 */
static void 
vfalse ()
{
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (int));
  CS ast = tmp;
  CCI CS ast = 0;
}

/* 
 * Integer division.
 */
static void 
div_yes ()
{
  int binar;
  binar = CCI CS (ast + sizeof (char *)) / (CCI CS ast);
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (int));
  CS ast = tmp;
  CCI CS ast = binar;
}

static void 
divui_yes ()
{
  unsigned int binar;
  binar = CCUI CS (ast + sizeof (char *)) / (CCUI CS ast);
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (unsigned int));
  CS ast = tmp;
  CCUI CS ast = binar;
}

static void 
divli_yes ()
{
  long int binar;
  binar = CCLI CS (ast + sizeof (char *)) / (CCLI CS ast);
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (long int));
  CS ast = tmp;
  CCLI CS ast = binar;
}

static void 
divlui_yes ()
{
  long unsigned int binar;
  binar = CCLUI CS (ast + sizeof (char *)) / (CCLUI CS ast);
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (long unsigned int));
  CS ast = tmp;
  CCLUI CS ast = binar;
}

/*
 * Double division.
 */
static void 
divd_yes ()
{
  double binard;
  binard = CCD CS (ast + sizeof (char *)) / (CCD CS ast);
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (double));
  CS ast = tmp;
  CCD CS ast = binard;
}

static void 
divld_yes ()
{
  long double binard;
  binard = CCLD CS (ast + sizeof (char *)) / (CCLD CS ast);
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (long double));
  CS ast = tmp;
  CCLD CS ast = binard;
}

/*
 * Float division.
 */
static void 
divf_yes ()
{
  float binarf;
  binarf = CCF CS (ast + sizeof (char *)) / (CCF CS ast);
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (float));
  CS ast = tmp;
  CCF CS ast = binarf;
}

#if 0
/*
 * Char division.
 */
static void 
divc_yes ()
{
  char binarc;
  binarc = CCC CS (ast + sizeof (char *)) / (CCC CS ast);
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (int));
  CS ast = tmp;
  CCC CS ast = binarc;
}
#endif

/*
 * Modulus.
 */
static void 
mod_yes ()
{
  int binar;
  binar = CCI CS (ast + sizeof (char *)) % (CCI CS ast);
  ast += sizeof (char *);
  move_stack_aligned (&tmp, -sizeof (int));
  CS ast = tmp;
  CCI CS ast = binar;
}

static void
move_stack_aligned (p, n)
  char **p;
  int n;
{
#ifdef HAVE_ASSERT_H
  assert ((n > 0 ? n : -n) <= MAX_CELL_MEMORY_SIZE);
#endif

  while ((long)*p % n)
    (n > 0) ? (*p += sizeof (char)) : (*p -= sizeof (char));
  *p += n;
}
