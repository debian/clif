/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992-1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
/*
 * store_context.h
 */

#ifndef _STORE_CONT_H
#define _STORE_CONT_H

/* \verbatim{store_cont_h_CONTEXT.tex} */

struct CONTEXT
{
  char *bp;
  char *frame;
  char *kodp;
  char *kodp1;
  char *kodp2;
  char *kodp3;
  char *kodp4;
  char *pc;
  char *stack;
  char *tmp;
  char *tmph;
#ifdef FLEX_SCANNER
  void *state;
#else
  int (*input) PROTO((void));
#endif
  struct CONTEXT *previous;
};

/* \verbatim */

#endif
