/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * xwin.c
 *
 * Hardware dependent graphical functions.
 */
#include "graph.c"

int cur_x, cur_y;

static void d_flush PROTO((void));
static void d_clear PROTO((int));
static void d_clear_text PROTO((int));
static void d_destroy_window PROTO((int));
static void d_destroy_window_write PROTO((int));
static void d_draw_text PROTO((int, int, int));
static void d_pause PROTO((int));


/*
 * Moves cursor to the specific position.
 */
static void
d_move (d_x, d_y)
  int d_x, d_y;
{
  cur_x = d_x;
  cur_y = d_y;
}

/* 
 * Draws a line from the cursor position to the specified point.
 */
static void
d_draw (d_handle, d_n, d_x, d_y)
  int d_handle, d_n, d_x, d_y;
{
#ifdef FORK_YES
  XDrawLine (disp, channel[d_handle].mypix,
	     channel[d_handle].member[d_n].set_gc, cur_x, cur_y, d_x,
	     d_y);
#endif
  XDrawLine (disp, channel[d_handle].mywin,
	     channel[d_handle].member[d_n].set_gc, cur_x, cur_y, d_x,
	     d_y);
}

/*
 * Draws a point.
 */
static void
d_point (d_handle, d_n, d_x, d_y)
  int d_handle, d_n, d_x, d_y;
{
#ifdef FORK_YES
  XDrawPoint (disp, channel[d_handle].mypix,
	      channel[d_handle].member[d_n].set_gc, d_x, d_y);
#endif
  XDrawPoint (disp, channel[d_handle].mywin,
	      channel[d_handle].member[d_n].set_gc, d_x, d_y);
}

static void 
d_flush ()
{
  XSync (disp, 1);
}

/*
 * Clears a window.
 */
static void
d_clear (d_handle)
  int d_handle;
{
  XClearWindow (disp, channel[d_handle].mywin);
#ifdef FORK_YES
  XFillRectangle (disp, channel[d_handle].mypix, default_gc, 0, 0,
		  channel[d_handle].w_resolution[0],
		  channel[d_handle].w_resolution[1]);
#endif
}

/*
 * Clears an alphanumerical window.
 */
static void
d_clear_text (d_handle)
  int d_handle;
{
  XClearWindow (disp, channel[d_handle].mywin_write);
#ifdef FORK_YES
  XFillRectangle (disp, channel[d_handle].mypix_write, default_gc, 0,
		  0, channel[d_handle].w_resolution[0],
		  channel[d_handle].w_resolution[1]);
#endif
}

/*
 * Destroys a graphical window.
 */
static void
d_destroy_window (d_handle)
  int d_handle;
{
  XDestroyWindow (disp, channel[d_handle].mywin);
#ifdef FORK_YES
  XFreePixmap (disp, channel[d_handle].mypix);
#endif
  XSync (disp, 1);
}

/*
 * Destroys an alphanumerical window.
 */
static void
d_destroy_window_write (d_handle)
  int d_handle;
{
  XDestroyWindow (disp, channel[d_handle].mywin_write);
#ifdef FORK_YES
  XFreePixmap (disp, channel[d_handle].mypix_write);
#endif
  XSync (disp, 1);
}

/*
 * Draws a text in an alphanumerical window.
 */
static void
d_draw_text (d_handle, d_x, d_y)
  int d_handle, d_x, d_y;
{
  XDrawText (disp, channel[d_handle].mywin_write, set_gc_write, d_x,
	     d_y, &(channel[d_handle].item), 1);
#ifdef FORK_YES
  XDrawText (disp, channel[d_handle].mypix_write, set_gc_write, d_x,
	     d_y, &(channel[d_handle].item), 1);
#endif
}

/*
 * Suspend graphical output and waits for a keystroke.
 */
static void
d_pause (d_handle)
  int d_handle;
{
#if 0
  printf ("d_pause\n");
#endif
  XSelectInput (disp, channel[d_handle].mywin, KeyPressMask);
  XWindowEvent (disp, channel[d_handle].mywin, KeyPressMask, &event);
#if 0
  printf ("event %d\n", event.type);
#endif
}
