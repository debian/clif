/*
 * define.h
 *
 * Clif's memory regions.
 */

#ifndef _DEFINE_H
#define _DEFINE_H

#ifndef BC		      /* Constant for size of the framework. */
#define BC 10
#endif


/* \verbatim{define_h_SIZE_SPACE.tex} */

#define SIZE_SPACE  210       /* Memory size in pages. */
#define SIZE_ARIT_STACK 7     /* Size of the arithmetical stack in pages. */
#define SIZE_TMP_STACK 52     /* Size of stack of temporaries in pages. */

/* \verbatim */

/* \verbatim{define_h_PAGE.tex} */

#define PAGE        512       /* Size of a page. */
#define SIZE_HAS   1999       /* Size of hash table. */
#define SIZE_HAS_LOC 401      /* Size of hash table for locals. */
#define SIZE_HAS_GOTO 257     /* Size of hash table for goto
				 labels. */
#define MAX_IDENT    1999     /* Max number of variables */
#define MAX_IDENT_LOC  401    /* Max number of local variables. */
#define SIZE_ADR_STACK 256    /* Size of address stack (obsolete). */
#define SIZE_STRUCT_FIX 256   /* Size of the stack of fixative structures. */ 
#define SIZE_REMOTE 1999      /* Size of hash table for remote function 
				 names. */

/* \verbatim */

#define MASKA 3		       /* Mask for arrays of type char. */


#endif /* _DEFINE_H */
