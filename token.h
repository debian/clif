typedef union	{
  int myint;
  unsigned int myuint;
  long int mylint;
  long unsigned int myluint;
  double mydouble;
  long double myldouble;
  float myfloat;
  char *mystring;
  wchar_t *mywstring;
  char mychar;
} YYSTYPE;
#define	NUMBERI	257
#define	NUMBERUI	258
#define	NUMBERLI	259
#define	NUMBERLUI	260
#define	NUMBERD	261
#define	NUMBERLD	262
#define	NUMBERF	263
#define	STRINGC	264
#define	WSTRINGC	265
#define	NUMBERC	266
#define	AUTO	267
#define	STATIC	268
#define	REGISTER	269
#define	EXTERN	270
#define	REMOTE	271
#define	UNLOAD	272
#define	INTRINSIC	273
#define	RPC	274
#define	IDENT	275
#define	INT	276
#define	DOUBLE	277
#define	FLOAT	278
#define	CHAR	279
#define	VOID	280
#define	LONG	281
#define	SHORT	282
#define	SIGNED	283
#define	UNSIGNED	284
#define	CONST	285
#define	VOLATILE	286
#define	ENUM	287
#define	STRUCT	288
#define	UNION	289
#define	EXIT	290
#define	IF	291
#define	ELSE	292
#define	SIZEOF	293
#define	SWITCH	294
#define	WHILE	295
#define	FOR	296
#define	DO	297
#define	CONTINUE	298
#define	BREAK	299
#define	RETURN	300
#define	GOTO	301
#define	CASE	302
#define	DEFAULT	303
#define	CSUSPEND	304
#define	RESUME	305
#define	EXPORT_T	306
#define	TYPEDEF	307
#define	TYPENAME	308
#define	MUL_ASSIGN	309
#define	DIV_ASSIGN	310
#define	MOD_ASSIGN	311
#define	ADD_ASSIGN	312
#define	SUB_ASSIGN	313
#define	LEFT_ASSIGN	314
#define	RIGHT_ASSIGN	315
#define	AND_ASSIGN	316
#define	XOR_ASSIGN	317
#define	OR_ASSIGN	318
#define	OR_A	319
#define	AND_A	320
#define	EQ_A	321
#define	NE_A	322
#define	LQ	323
#define	GQ	324
#define	SHIL	325
#define	SHIR	326
#define	NEG_T	327
#define	NEG_B	328
#define	PP	329
#define	MM	330
#define	PTR	331


extern YYSTYPE yylval;
