The remote procedure call slot that we have built currently into the
interpreter supports a remote procedure call paradigm, where client
(or caller) waits until the finish of the remote procedure. We would
like to improve this mechanism with a possibility of concurrent
computation and synchronization via internal mechanism which would
allow the interpreter to proceed with a computation even if the data
from the remote call are not at the disposal. The interpreter runs
until there is no reference to the results of the remote call and then
blocks just before the first such reference. (There is possibility of
changing syntax of the language).

We suppose to implement the following special atomary types: integers
with arbitrary precision, matrices with a dynamical allocation of
memory and splines.

To allow a more flexibility in language syntax we propose a procedure
where for each stage of the compiler development there are two
compilers. One is the active compiler with a full semantic and the
other one is the same parser but with a special semantic which is
directed to a source-source translation towards a new syntax.

We also think about a possibility to conduct a research towards a
compiler with a unified parser working in a heterogeneous environment
with different code generation parts for particular environments. We
have in mind to apply the methods used in object programming to solve
"inheritance anomaly" for this problem.  When we look at the parser
constructed with YACC it can be seen that parsing and code generation
parts are interspersed in such a way that it is not possible to
separate and encapsulate those parts from each other. This is an
instance of inheritance anomaly which can be approached with recently
discovered methods.
