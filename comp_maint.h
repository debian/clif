/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * comp_maint.h
 */

#ifndef _COMP_MAINT_H
#define _COMP_MAINT_H

/* Process implicit cast in expressions. */
extern int implicit_cast PROTO((void));
/* Process cast to l_value of an expresion. */
extern int l_value_cast PROTO((void));

/* If return occurred it saves the backpatch address of the JMP
 instruction. */ 
extern void retfix PROTO((void));

/* If continue occurred it saves the backpatch address of the JMP
   instruction. */
extern int cofix PROTO((void));

/* If break occurred it saves the backpatch address of the JMP
   instruction. */
extern int brfix PROTO((void));

/* Backpatches the address of continue in while loop. */
extern void fix_cont_w PROTO((void));

/* Backpatches the address of continue in for loop. */
extern void fix_cont_f PROTO((void));

/* Backpatches the address of break in while loop. */
extern void fix_break_w PROTO((void));

/* Backpatches the address of break in for loop. */
extern void fix_break_f PROTO((void));

/* Backpatches the address of a return statement. */
extern void fix_ret PROTO((void));

/* Backpatches the address of break in for loop. */
extern void fix_break_s PROTO((void));

extern int add_to_spec_list PROTO((void));
extern int add_to_ident_list PROTO((void));
extern void search_duplicate_labels PROTO((void));
extern int add_constant_to_list PROTO((int));
extern void add_default_to_fixp PROTO((void));
extern int yyerror PROTO((char *));
extern void func_def_s PROTO((void));
extern void func_def_r PROTO((void));
extern void enter_scope PROTO((void));
extern void exit_scope PROTO((void));

extern int scope_level;
extern int verbose;

extern void move2lvalue PROTO((void));

/* Map function for array subscripts. */
extern void array_subscript PROTO((struct internal_type *));

/* Clears internal_type part of the */
/* hash table for local variables. */
extern void clear_internal_type PROTO((struct internal_type *));

/*
 * Saves dimensions of arrays in the hash table.
 */
extern void put_array_subscript PROTO((struct internal_type **));

/* Compares two trees, if there are */
/* equivalent. */
extern int compare2trees PROTO((struct internal_type *, struct internal_type *));

extern int type_transform PROTO((enum intern_arit_class, enum intern_func_class, int));

/* Main function. */
extern void clif       PROTO((int, char *[]));

/* Checking if labels in case */
/* statement are different. */
extern void search_duplicate_labels PROTO((void));

/* Adding integer constant to list if */
/* it is in switch statement. */
extern int add_constant_to_list PROTO((int));

/* Adding default label to the fixative */
/* structure. */
extern void add_default_to_fixp PROTO((void));

/* Source line number. Detecting if */
/* the current line was already */
/* printed. Using in error messages. */
extern int source_line_number;

/* Line number where the error was. */
extern int error_line_number;

/* Count of errors. */
extern int error_count;

#define COMPILE_ONLY no_compile_only = 0
#define COMPILE_P !no_compile_only

extern struct internal_type 
*l_type_spec;
				/* List of type specifiers in function
                                   declaration. */
extern struct ident_list_str
*ident_list;
				/* List of formal parameters. */

/* Combination of all hashing tables. Choose the appropriate one and
   put the variable there. */
extern void put2table PROTO((char *, struct internal_type *));

/* Number of args for a function is returned. */
extern int get_num_args PROTO((struct internal_type *));

/* Type promotion. */
extern void promote_type PROTO((void));

extern void check_spec_constr PROTO((struct internal_type *, char *));
extern void gen_cast_needed PROTO((void));

#endif /* COMP_MAINT_H */
