/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
#ifndef _CONFIG_H
#define _CONFIG_H

#include "auto-config.h"

#ifdef HAVE_ASSERT_H
#include <assert.h>
#ifndef MAX_CELL_MEMORY_SIZE
#define MAX_CELL_MEMORY_SIZE sizeof (long double)
#endif
#endif

#if HAVE_LONG_DOUBLE == 0 
#ifdef STDC_HEADERS
#include <float.h>
#if DBL_MANT_DIG == LDBL_MANT_DIG && DBL_DIG == LDBL_DIG && 		\
DBL_MIN_EXP == LDBL_MIN_EXP && DBL_MIN_10_EXP == LDBL_MIN_10_EXP && 	\
DBL_MAX_EXP == LDBL_MAX_EXP && DBL_MAX_10_EXP == LDBL_MAX_10_EXP
#define LDOUBLE_EQ_DOUBLE
#endif
#endif
#endif

#ifdef HAVE_LIMITS_H
#include <limits.h>

#if INT_MAX == LONG_MAX && INT_MIN == LONG_MIN
#define LONG_EQ_INT
#endif

#if INT_MAX == SHRT_MAX && INT_MIN == SHRT_MIN
#define SHORT_EQ_INT
#endif

#if UINT_MAX == USHRT_MAX
#define UINT_EQ_USHORT
#endif

#if UINT_MAX == ULONG_MAX
#define UINT_EQ_ULONG
#endif

#if LONG_MAX >= UINT_MAX
#define LONG_CAN_UINT
#endif

#endif

typedef union {
  int *a;
  long int *b;
  long unsigned int *c;
  short int *d;
  short unsigned int *e;
  char *f;
  signed char *g;
  unsigned char *h;
  double *i;
  long double *j;
  float *k;
  void (*l)();
}  pointer_size;

#define POINTER_SIZE sizeof (pointer_size)
#endif
