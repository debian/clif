/* C code produced by gperf version 2.1 (K&R C version) */
/* Command-line: gperf -t -p -k 2,3 ./keyword.gperf  */


#include "config.h"
#ifdef STDC_HEADERS
#include <stddef.h>
#endif
#include "token.h"
struct el_mc {
  char *name;
  int kval;
};

#define MIN_WORD_LENGTH 2
#define MAX_WORD_LENGTH 11
#define MIN_HASH_VALUE 2
#define MAX_HASH_VALUE 96
/*
   39 keywords
   95 is the maximum key range
*/

static int
hash (str, len)
     register char *str;
     register unsigned int  len;
{
  static unsigned char hash_table[] =
    {
     96, 96, 96, 96, 96, 96, 96, 96, 96, 96,
     96, 96, 96, 96, 96, 96, 96, 96, 96, 96,
     96, 96, 96, 96, 96, 96, 96, 96, 96, 96,
     96, 96, 96, 96, 96, 96, 96, 96, 96, 96,
     96, 96, 96, 96, 96, 96, 96, 96, 96, 96,
     96, 96, 96, 96, 96, 96, 96, 96, 96, 96,
     96, 96, 96, 96, 96, 96, 96, 96, 96, 96,
     96, 96, 96, 96, 96, 96, 96, 96, 96, 96,
     96, 96, 96, 96, 96, 96, 96, 96, 96, 96,
     96, 96, 96, 96, 96, 96, 96, 40, 96,  0,
     96,  0,  5, 15, 15, 40, 96, 96, 25, 45,
     10,  0,  0, 96,  5, 20,  0, 25, 96, 50,
     30, 10, 10, 96, 96, 96, 96, 96,
    };
  register int hval = len;

  switch (hval)
    {
      default:
      case 3:
        hval += hash_table[str[2]];
      case 2:
        hval += hash_table[str[1]];
    }
  return hval ;
}

struct el_mc *
in_word_set (str, len)
     register char *str;
     register unsigned int len;
{

  static struct el_mc  wordlist[] =
    {
      {"",}, {"",}, 
      {"do",  DO},
      {"rpc",  RPC},
      {"goto",  GOTO},
      {"",}, 
      {"return",  RETURN },
      {"if",  IF},
      {"for",  FOR},
      {"",}, 
      {"break",  BREAK},
      {"struct",  STRUCT},
      {"default",  DEFAULT},
      {"int",  INT},
      {"long",  LONG},
      {"const",  CONST},
      {"",}, 
      {"typedef",  TYPEDEF},
      {"continue",  CONTINUE},
      {"intrinsic",  INTRINSIC},
      {"short",  SHORT},
      {"",}, {"",}, 
      {"register",  REGISTER},
      {"",}, {"",}, 
      {"resume",  RESUME},
      {"",}, {"",}, 
      {"auto",  AUTO},
      {"float",  FLOAT},
      {"double",  DOUBLE},
      {"",}, 
      {"volatile",  VOLATILE},
      {"",}, {"",}, 
      {"extern",  EXTERN},
      {"",}, 
      {"unsigned",  UNSIGNED},
      {"enum",  ENUM},
      {"",}, 
      {"export_type",  EXPORT_T},
      {"",}, {"",}, 
      {"void",  VOID},
      {"",}, 
      {"static",  STATIC},
      {"",}, {"",}, 
      {"else",  ELSE},
      {"",}, 
      {"remote",  REMOTE},
      {"",}, 
      {"csuspend",  CSUSPEND},
      {"",}, 
      {"union",  UNION},
      {"sizeof",  SIZEOF},
      {"",}, {"",}, 
      {"char",  CHAR},
      {"while",  WHILE},
      {"signed",  SIGNED},
      {"",}, {"",}, 
      {"case",  CASE},
      {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, 
      {"exit",  EXIT},
      {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, 
      {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, {"",}, 
      {"",}, {"",}, {"",}, 
      {"switch",  SWITCH},
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= MIN_HASH_VALUE)
        {
          register char *s = wordlist[key].name;

          if (*s == *str && !strcmp (str + 1, s + 1))
            return &wordlist[key];
        }
    }
  return 0;
}
