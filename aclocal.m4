AC_DEFUN(CLIF_SVR_SIGS,
[AC_MSG_CHECKING(for SVR signals)
 AC_CACHE_VAL(clif_cv_sigs,
 [AC_TRY_LINK(
    [#include<stdio.h>
    #include<$op_signal>
    #include<$op_termio>
    #include<$op_fcntl>],
    [sigset(SIGINT,(void *)NULL);],
    clif_cv_sigs=yes,
    clif_cv_sigs=no)])
AC_MSG_RESULT($clif_cv_sigs)])

AC_DEFUN(CLIF_SVR3_TERM,
[AC_MSG_CHECKING(for SVR3 features)
 AC_CACHE_VAL(clif_cv_lnctl,
    [AC_TRY_LINK(
	[#include<$op_termio>],
	[int x = LNEW_CTLECH;],
	clif_cv_lnctl=yes,
	clif_cv_lnctl=no)])
AC_MSG_RESULT($clif_cv_lnctl)])

AC_DEFUN(CLIF_BSD_SIG,
[AC_MSG_CHECKING(for old-BSD signals)
 AC_CACHE_VAL(clif_cv_sigv,
    [AC_TRY_LINK([#include<stdio.h>
	#include<$op_signal>
	#include<$op_termio>
	#include<$op_fcntl>],
	[struct sigvec vec,ovec; sigvec(SIGINT,&vec,&ovec);],
	clif_cv_sigv=yes,
	clif_cv_sigv=no)])
AC_MSG_RESULT($clif_cv_sigv)])

AC_DEFUN(CLIF_LINUX_SIG,
[AC_MSG_CHECKING(for Linux signals)
 AC_CACHE_VAL(clif_cv_lsig,
    [AC_TRY_COMPILE([#include</usr/include/stdio.h>
     #include</usr/include/bsd/signal.h>
     #include</usr/include/$op_termio>
     #include</usr/include/$op_fcntl>],
    [struct sigvec vec,ovec; sigvec(SIGINT,&vec,&ovec);],
    ADD_OBJS="$ADD_OBJS inter_handl_lin.o"
    ADD_FORK="$ADD_FORK \$(srcdir)/sigig_lin.c" 
    ADD_IDIR="$ADD_IDIR -I/usr/include" 
    AC_TRY_LINK([#include</usr/include/stdio.h>
	#include</usr/include/bsd/signal.h>
	#include</usr/include/$op_termio>
	#include</usr/include/$op_fcntl>],
	[ioctl (stdin, TIOCSTI, "\n");],
	AC_DEFINE(HAVE_TIOCSTI))
	clif_cv_lsig=yes,
	clif_cv_lsig=no)])
AC_MSG_RESULT($clif_cv_lsig)])

AC_DEFUN(CLIF_POSIX_SIG,
[AC_MSG_CHECKING(if it is a true POSIXized system)
 AC_CACHE_VAL(clif_cv_psig,
    [AC_TRY_COMPILE([#include <stdio.h>
	#include <$op_signal>
	#include <$op_termio>],
	[struct termios term;
	 struct sigaction act, oact;
	 int fd;
	 tcgetattr (fd, &term);
	 sigemptyset (&act.sa_mask);
	 sigaction (SIGINT, &act, &oact);],
	 clif_cv_psig=yes,
	 clif_cv_psig=no)])
AC_MSG_RESULT($clif_cv_psig)])

AC_DEFUN(CLIF_MAX_ALIGNMENT,
[AC_MSG_CHECKING(for default alignment)
 AC_CACHE_VAL(clif_cv_align,
    [AC_TRY_RUN(
	[#include <stdio.h>
	 struct foo {
	    int a;
	    double b;};
	 int main () {
	    exit ((int)&((struct foo *)0)->b != sizeof (int));}],
	clif_cv_align=4,
	clif_cv_align=8,
	clif_cv_align=8)])
 if test $clif_cv_align -eq 4; then
    AC_DEFINE(MAX_ALIGNMENT, 4)
 else
    AC_DEFINE(MAX_ALIGNMENT, 8)
 fi
AC_MSG_RESULT($clif_cv_align)])
