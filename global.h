/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * global.h
 *
 * Declaration of global variables.
 */


#ifndef _GLOBAL_H
#define _GLOBAL_H

#include <stdio.h>
#if __STDC__
#include <stdarg.h>
#else
#include <varargs.h>
#endif
#include "ansidecl.h"

extern int s;                /* Number of opened files. */ 
extern int line_counter,char_counter; /* Counter of lines and characters. */
extern char line_buf[];		/* Contains whole line during lexical */
				/* analysis and compiling. */
extern char *mem_start;        /* Pointer to the memory. */
extern char *stack;            /* Stack pointer. */
extern char *bp;  	       /* Base pointer.
				* Used for relative addressing of variables.
				*/
extern char *frame;            /* Frame pointer.
				* Used for parameter passing to
				* remote functions.
				*/
extern char **ast;             /* Arithmetic stack pointer. */
extern char *pc;               /* Program counter.
				* Points to the next executed instruction.
				*/
extern char *fixst;    	       /* Pointer to the fixative stack. */
#if 0
extern char **adr_stack;	/* Pointer to the address stack (obsolete). */
#endif
extern char *tmp;	       /* Pointer to the stack of temporaries. */
extern char *tmph;	       /* Pointer to the current level beginning
				* of the stack of temporaries.
				* It is used for clearing the stack.
				*/
extern char *tmp_start;	       /* Pointer to the beginning of the stack
				* of temporaries.
				*/
extern char *proc_name[];      /* Address of function beginning. */
extern char *proc_name_text[]; /* Pointer to names of remote functions. */
extern char *struct_union_enum_name[]; /* Names of tags. */

extern struct CONTEXT *context;
extern int proc;		/* Counter for function call.  Used by
				 * nested calls when function is used as
				 * parameter.  */
extern int suen_count;		/* struct_union_enum_name
				   counter. Nesting levels of tag
				   declaration. */

extern struct internal_type
*type_com[];			/* Complex type of an operand in an expression
				 * (simple, array).
				 */
extern struct remote_has_tab *hastab_remote;
                              /* Pointers to the hash table of remote
			       * functions.
			       */
extern int dim[];	      /* Max size of array dimensions (256). */
extern int poc;		      /* Counter of the number of dimensions
			       * in arrays.
			       */
extern int remote_flag;       /* Flag for remote function call. */
extern int body_flag;         /* Flag of a variable body. */
extern int typedef_f;	      /* If in the context can be TYPENAME
				 token equals 1; 0 otherwise. */
extern int enum_value;		/* Value of the enumeration specifier. */

extern struct internal_type *typeh[];
extern int type_spec_count;

extern struct remote_tab 
*remote_ptr_C;                /* Pointer to the structure of remote
			       * function table.
			       */

extern struct return1 *rp;    /* Pointer for backpatch address of
			       * return in a function.
			       */
extern char *kodp;	/* Pointer to the generated code.  */
extern char *kodp1;		/* Pointer for sweeping out unuseful */
				/* code from the memory.  */
extern char *kodp2;		/* Pointer for remembering context */
				/* used during context switching in */
				/* interrupts. */
extern char *kodp3;		/* Pointer for fixing returns in */
				/* functions. */
extern char *kodp4;		/* If there is l_value throw away last
				   PUSH instruction. */

/* Pointers to the last and current generated instruction. */
extern char *last_instr, *current_instr;

extern int count[], count_arr;
extern int handler;	      /* Interrupt service. */
extern int virtual_machine_suspended;
extern int clif_interrupt_level;


extern int set;
extern union fix *fixp;		/* Pointer to the fixative stack. */
extern char *call_fix[];	/* Address' backpatching during */
				/* parameter passing. */
#if 0
char *say();
#endif
extern char *text;

extern void error_message PVPROTO((int, ...));

#ifndef MSDOS
extern void interrupt_service_sync PROTO((void));
#endif
#ifndef NOT_MSWIN_AND_YES_DOS
extern void store_context PROTO((void));
extern void restore_context PROTO((void));
#endif

typedef struct fileattr FILEATTR;

struct fileattr
{
  FILE *fp;
  char *name;
  int line_counter;
};

#ifndef MSDOS
#define HANDLER_TEST handler
#define HANDLER_SET handler = 0
#define VIRTUAL_MACHINE_SUSPENDED_SET virtual_machine_suspended = 1
#define VIRTUAL_MACHINE_SUSPENDED_RESET virtual_machine_suspended = 0
#define CLIF_INTERRUPT_LEVEL_INCREMENT ++clif_interrupt_level
#define CLIF_INTERRUPT_LEVEL_DECREMENT (clif_interrupt_level<=0?clif_interrupt_level:--clif_interrupt_level)
#endif

#ifdef MSWIN
#define HANDLER_TEST handler_t()
#define HANDLER_SET handler = 0
#define VIRTUAL_MACHINE_SUSPENDED_SET virtual_mach_sus_ss()
#define VIRTUAL_MACHINE_SUSPENDED_RESET virtual_mach_sus_rr()
#define CLIF_INTERRUPT_LEVEL_INCREMENT inter_level_p()
#define CLIF_INTERRUPT_LEVEL_DECREMENT inter_level_m()
#endif

/* Used only for debugging purposes. For compiling this use -DDEBUG
   Checks kodp for right value.  kodp is used by code generetion as
   pointer to the end of the generated code.  */

#ifdef DEBUG
#define GENCODE printfx ("kodp=%u\n", kodp);	\
last_instr = current_instr;			\
current_instr = kodp;
#else
#define GENCODE last_instr = current_instr;	\
current_instr = kodp;
#endif

				/* Checks stack overflow.
			         * Only if -DCHKSTACK is defined during
			         * compilation.
			         */
#ifdef CHKSTACK
#define check_stack() if (stack <= kodp) \
{ \
  error_message (4001); \
  return (-2); \
}
#else
#define check_stack()
#endif
#endif /* _GLOBAL_H */
