/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * graph.c
 *
 * Graphic primitives.
 */

#include <math.h>

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

static void d_move PROTO((int, int));
static void d_draw PROTO((int, int, int, int));
static void d_point PROTO((int, int, int, int));

static void window PROTO((int, int, double, double, double, double));
static void move PROTO((int, int, double, double));
static void draw PROTO((int, int, double, double));
static void draw_point PROTO((int, int, double, double));


/* \verbatim{graph_c_window.tex} */

/*
 * Creates a window from user's coordinates.
 * Counts world coordinates.
 * handle - is handle of the window
 * n - specifies line in the window (user can have more lines in the window)
 * rest are coordinates
 */
static void
window (handle, n, x_left, y_down, x_right, y_up)
     int handle, n;
     double x_left, y_down, x_right, y_up;
{
  channel[handle].member[n].ax = 
    (channel[handle].w_resolution[0] - 1) / (fabs(x_left - x_right));
  channel[handle].member[n].ay = 
    (channel[handle].w_resolution[1] - 1) / (fabs(y_down - y_up));
}

/* \verbatim */

/* \verbatim{graph_c_move.tex} */

/*
 * Moves cursor to the specified position.
 * handle - is handle of the window
 * n - specifies line in the window (user can have more lines in the window)
 * x, y - coordinates
 */
static void
move (handle, n, x, y)
     int handle, n;
     double x, y;
{
  d_move((int)floor ((x - channel[handle].start_time)
		     * channel[handle].member[n].ax),
	 channel[handle].w_resolution[1] - 1
	 - (int)floor ((y - channel[handle].member[n].lower)
		       * channel[handle].member[n].ay));
}

/* \verbatim */

/* \verbatim{graph_c_draw.tex} */

/*
 * Draws a line from the cursor position to the point of coordinates.
 * handle - is handle of the window
 * n - specifies line in the window (user can have more lines in the window)
 * x, y - coordinates
 */
static void 
draw (handle, n, x, y)
  int handle, n;
  double x, y;
{
  d_draw (handle, n,
	  (int)floor ((x - channel[handle].start_time)
		      * channel[handle].member[n].ax),
	  channel[handle].w_resolution[1] - 1
	  - (int)floor ((y - channel[handle].member[n].lower)
			* channel[handle].member[n].ay));
}

/* \verbatim */

/* \verbatim{graph_c_draw_point.tex} */

/*
 * Draws a point from the coordinates.
 * handle - is handle of the window
 * n - specifies line in the window (user can have more lines in the window)
 * x, y - coordinates
 */
static void
draw_point (handle, n, x, y)
  int handle, n;
  double x, y;
{
  d_point (handle, n,
	   (int)floor ((x - channel[handle].start_time)
		       * channel[handle].member[n].ax),
	   channel[handle].w_resolution[1] - 1
	   - (int)floor ((y - channel[handle].member[n].lower)
			 * channel[handle].member[n].ay));
}

/* \verbatim */
