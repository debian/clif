%{
/* 
 * channel.l
 *
 * lex source for channels
 * used as compiler for graphical interface
 *
 */

#include<string.h>
#ifdef input
#undef input
#undef yywrap
#endif
#include"mystdio.h"
#include"buf.h"
#include"ch-parser.h"
#include"init_ch.c"

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

#if 0
extern char *callocx PROTO((unsigned, unsigned));
#endif
extern char *string PROTO((char *));
extern void error_message PROTO((int));
%}
mes	["]
newline	[\n]
tab	[ \t]
cifra	[0-9]
exp	[Ee][-+]?{cifra}+
alfach	[A-Z_a-z0-9]+
numberi	[+-]?{cifra}+
numberd ([+-]?{cifra}*"."{cifra}+({exp})?)|([+-]?{cifra}+"."{cifra}*({exp})?)|([+-]?{cifra}+{exp})
%%
{tab}		{}
{newline} 	{;}
fields		{return (FIELDS);
			/* 
			 * Number of records per channel
                         */}
type    	{return (TYPE);
			/* 
			 * Channel type (graphic)
 			 * now implemented only graphic channel
			 */}
print_format	{return (PRINT_FORMAT);
			/* 
			 * point
			 * line
  			 */}
direction	{return (DIRECTION);
			/*
			 * input or output
			 * graphic channel implemented only as output channel
			 */}
start_time	{return (START_TIME);}
duration_time	{return (DURATION_TIME);
			/* 
			 * Length of the window in number of records
			 */}
w_resolution	{return (W_RESOLUTION);
			/* 
			 * Size of the windows
			 */}
lower		{return (LOWER);
			/* 
			 * Lower bound of the window
			 */}
upper		{return (UPPER);
			/* 
			 * Upper bound of the window
			 */}
style		{return (STYLE);
			/* 
			 * Line type 
			 * now implemented as color setting 
			 */}
OnLeaveW	{return (ON_LEAVE_WINDOW);
		 	/*
			 * Suspend output tto the window temporarily
			 * To continue press any key
			 */}
automatic	{return (AUTOMATIC);
			/*
			 * automatic for duration_time and start_time
			 */}
{numberi}	{sscanf (yytext, "%d", &yylval.myint); return (NUMBERI);
			/* 
			 * Integer number
			 */}
{numberd}	{sscanf (yytext, "%lf", &yylval.mydouble); return (NUMBERD);
			/* 
			 * Double number
			 */}
{alfach}	{yylval.mystring = string (yytext);
		 return(STRING);
			/*
			 * String
			 */}
.		{return(*yytext);}
%%
