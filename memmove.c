/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * memmove.c
 *
 * Definition of an independent memmove function.
 */

#include "global.h"
#include "cast.h"
#include "memmove.h"

void 
*memmovex(s1, s2,  n)
  void *s1, *s2;
  unsigned int n;
{
  int i;
  for (i = n / sizeof(int) - 1; i >= 0; --i)
    CS ((int *)s1 + i) = CS ((int *)s2 + i);

  return (s1);
}
