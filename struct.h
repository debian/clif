/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * struct.h
 */

#ifndef _STRUCT_H
#define _STRUCT_H


/* \verbatim{struct_h_tab.tex} */

struct tab                     /* Hash table structure. */
{
  char *name;
  int def;			/* Position in the hash table. */
  int count;			/* Is the variable used at all? */
  int use_line_number;		/* The first use of the variable. */
  int l_value_flag;		/* Is the variable initialized before
				   the use? */
  int declaration_line;		/* Variable declaration line number. */
  struct tab *next;
}; 

/* \verbatim */

/* \verbatim{struct_h_dim.tex} */

struct range
{
  int lower;
  int upper;
};


/* \verbatim */

/* \verbatim{struct_h_FIX.tex} */

struct FIX		      /* List of addresses where to backpatch
			       * undefined function.
			       */
{
  char *address;
  struct FIX *next;
};

/* \verbatim */

/* \verbatim{struct_h_attr.tex} */
				/* Type attribute. */
struct attr
{
  enum intern_func_class function_class;
  int export_type;
  enum type_qual type_qualifier;
  enum storage_class_specifier storage_class_specifier;
  enum intern_arit_class arit_class;
  int memory_size;
  char *domain;
};

/* \verbatim */

/* \verbatim{struct_h_internal_type.tex} */
				/* Internal type structure. */
struct internal_type
{
  struct internal_type *input;
  struct internal_type *arity;
  char *field_name;
  int offset;
  struct attr attribute;
  struct internal_type *output;
};

/* \verbatim */

struct ident_list_str
{
  char *ident;
  struct ident_list_str *next;
} ;


/* \verbatim{struct_h_ident_tab.tex} */

struct ident_tab              /* Table of identifiers. */
{
  struct internal_type *type;
  int body;
  struct ident_list_str *list_formal_param;
  struct FIX *next;
  char *adr;
};

/* \verbatim */

/* \verbatim{struct_h_ident_tab_loc.tex} */

struct ident_tab_header
{
  int scope_level;
  int pi_loc;
  int offset;
  char *file_scope;
  struct ident_tab_header *previous_level;
  struct ident_tab_loc *all;
  struct tab *hastab_loc;
  struct ident_tab_loc *table;
};

struct ident_tab_loc          /* Table of local identifiers. */
{
  struct internal_type *type;
  int body;
  int offset;
  char *adr;
  struct ident_list_str *list_formal_param;
  struct ident_tab_loc *previous;
};

/* \verbatim */

/* \verbatim{struct_h_return1.tex} */

struct return1		      /* List of addresses where to backpatch 
			       * returns from a function.
			       */
{
  char *adr;
  struct return1 *next;
};

/* \verbatim */

/* \verbatim{struct_h_remote_tab.tex} */

struct remote_tab             /* Hash table structure for intrinsic 
			       * functions.
			       */
{
  char *name;
  void (*adr) PROTO((char **));
  struct remote_tab *next;
};

/* \verbatim */

/* \verbatim{struct_h_remote_has_tab.tex} */

struct remote_has_tab         /* Hash table structure for intrinsic
			       * functions.
			       */
{
  char *name;
  int offset;
  struct remote_has_tab *next;
};

/* \verbatim */
/* \verbatim{struct_h_goto_tab.tex} */

struct goto_adr			/* List of addresses with goto
				   statements for the current label. */
{
  char *adr;			/* Address of the goto statement. */
  int line_number;		/* Line number of the goto statement. */
  struct goto_adr *gnext;
};


struct goto_tab			/* Hash table structure for goto
				   labels. */
{
  char *name;			/* Name of the label. */
  char *label_adr;		/* Address of the label in the
				   generated code. */
  int line_number;		/* Line number of the label in the
				   source file. */
  struct goto_adr *gnext;	/* List of goto's to the label. */
  struct goto_tab *next;
};

/* \verbatim */


#endif /* _STRUCT_H */
