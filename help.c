/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * help.c
 *
 * Prints out a short help file.
 */

#include <stdio.h>

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

extern int fprintfx PROTO((FILE *, char *, ...));
extern int getcx PROTO((FILE *));
extern char version[];
extern int verbose;

int help PROTO((int));

int 
help (typ)
  int typ;
{
  FILE *spf;
  int znak;

  switch(typ)
    {
    case 1:
      if((spf = fopen ("small.hlp", "r")) == NULL)
	{
	  fprintfx (stderr, "\tcopy file small.hlp in this directory\n");
	  return (-1);
	}
      else
	{
	  while ((znak = getcx (spf)) != EOF) 
	    putc ((char)znak, stdout);
	}
      break;
    case 2:
      if((spf = fopen ("copying", "r")) == NULL)
	{
	  fprintfx (stderr, "\tcopy file copying in this directory\n");
	  return (-1);
	}
      else
	{
	  while ((znak = getcx (spf)) != EOF) 
	    putc ((char)znak, stdout);
	}
      break;
    case 3:
            if ((spf = fopen ("warranty", "r")) == NULL)
	{
	  fprintfx (stderr, "\tcopy file warranty in this directory\n");
	  return(-1);
	}
      else
	{
	  while ((znak = getcx (spf)) != EOF) 
	    putc ((char)znak, stdout);
	}
      break;
    case 4:
      verbose = 1;
      break;
    case 5:
      fprintfx (stderr, "clif version %s\n", version);
      return (-1);
    }
  return (0);
}

