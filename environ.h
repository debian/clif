#ifndef _ENVIRON_H
#define _ENVIRON_H

#define PATH_SEPARATOR ':'
#define DIRECTORY_SEPARATOR '/'
#define STRING_TERMINATOR '\0'
#define EXTENSION ".cl"
#define CLIF_INCLUDE CLIF_INCLUDE


#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

void init_env PROTO((void));
char *get_next_path PROTO((void));

#endif
