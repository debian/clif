#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "environ.h"

static char *clif_env[256];
static int count = 0;

void
init_env ()
{
  char *env, *endp;
  int i = 0;
  
  env = getenv ("CLIF_INCLUDE");
  while (NULL != (endp = strchr (env, PATH_SEPARATOR)))
    {
      *endp = STRING_TERMINATOR;
      clif_env[i++] = env;
      env = endp + 1;
    }
  clif_env[i] = env;
  clif_env[i + 1] = NULL;
}


char *
get_next_path ()
{
  if (NULL == clif_env[count])
    {
      count = 0;
      return ((char *) NULL);
    }
  else
    {
      return (clif_env[count++]);
    }
}

int
check_valid_name (name)
     char *name;
{
  int n = strlen (name);
  
  if ('l' == name[n - 1] && 'c' == name[n - 2] && '.' == name[n - 3])
    {
      return (1);
    }
  else
    {
      return (0);
    }
}

