%{
/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
/*
 * run_str.l
 *
 * lex source for run string analysis
 */
#include<stdio.h>
#include<string.h>
#ifdef input
#undef input
#undef yywrap
#endif
#include"buf.h"
#include"rs-parser.h"
#include"init_rs.c"

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

#ifdef FLEX_SCANNER
#define YY_NO_UNPUT
#endif

extern char *string PROTO((char *));
%}
cifra [0-9]
number {cifra}+
newline [\n]
tab	[ \t]
becka	[Bb]
cecka	[Cc]
h	[Hh]
e	[Ee]
l	[Ll]
p	[Pp]
v       [Vv]
w       [Ww]
copying [Cc][Oo][Pp][Yy][Ii][Nn][Gg]
warranty [Ww][Aa][Rr][Rr][Aa][Nn][Tt][Yy]
verbose [Vv][Ee][Rr][Bb][Oo][Ss][Ee]
version [Vv][Ee][Rr][Ss][Ii][Oo][Nn]
alfa	[A-Z_a-z0-9\.\/\\~]
alfars  {alfa}({alfa}|\-)*
%%
{tab}		{}
{newline} 	{;}
{number}	{sscanf (yytext, "%d", &yylval.myyint); return (NUMBER);
			 /* 
 			  * Matches an integer number.
 			  */}
\-{becka}{cecka} {return (BGC);}
\/{becka}{cecka} {return (BGC); 
			 /* 
			  * Redefining of size of framework memory.
			  */}
\-dy		{return (DUMP_YACC);}
\-fcall\-by\-reference {return (CALL_BY_REFERENCE);}
\-fno\-call\-by\-reference {return (NO_CALL_BY_REFERENCE);}
\-fcall\-by\-value {return (CALL_BY_VALUE);}
\-fno\-call\-by\-value {return (NO_CALL_BY_VALUE);}
\-fhandle\-main {return (HANDLE_MAIN);}
\-g		{return (DBG_INFO);}
\--{h}{e}{l}{p}	{return (HELP);}
\-{h}{e}{l}{p}	{return (HELP);}
\/{h}{e}{l}{p}	{return (HELP);
			 /* 
			  * Starts a help.
			  */}
\-{h}		{return (HELP); /* Starts a help. */}
\/{h}		{return (HELP); /* Starts a help. */}
\-\?		{return (HELP); /* Starts a help */}
\/\?		{return (HELP); /* Starts a help */}
\/{cecka}	{return (COPYING); /* Starts copying policy */}
\--{copying}    {return (COPYING);}
\-{copying}	{return (COPYING);}
\-w		{return (WARNING_INHIBIT); /* Starts warranty */}
\/{w}		{return (WARRANTY); /* Starts warranty */}
\--{warranty}	{return (WARRANTY);}
\-{warranty}	{return (WARRANTY);}
\-c		{return (COMPILE); /* Starts compiling only */}
\-W		{return (WARNING_EXTRA);}
\-Waggregate\-return {return (WARNING_AGGREGATE_RETURN);}
\-Wcomment	{return (WARNING_COMMENT);}
\-Werror	{return (WARNING_ERROR);}
\-Wformat	{return (WARNING_FORMAT);}
\-Wimplicit	{return (WARNING_IMPLICIT);}
\-Wreturn\-type {return (WARNING_RETURN_TYPE);}
\-Wtrigraphs	{return (WARNING_TRIGRAPHS);}
\-Wunitialized	{return (WARNING_UNINITIALIZED);}
\-Wunused	{return (WARNING_UNUSED);}
\-Wall		{return (WARNING); /* Prints warnings as well */}
\--{v}		{return (VERSION); /* Prints version number */}
\-{v}		{return (VERSION); /* Prints version number */}
\--{version}	{return (VERSION); /* Prints version number */}
\-{version}	{return (VERSION); /* Prints version number */}
\--{verbose}	{return (VERBOSE); /* Prints message about environment */}
\-{verbose}	{return (VERBOSE); /* Prints message about environment */}
{alfars}	{/*
		  * Other run string parameters.
		  */
		 yylval.myystring = string (yytext);
		 return(STRING);}
.		{return (*yytext);}
%%
