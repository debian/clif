/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/* \verbatim{intrinsic_h.tex} */

/*
 * intrinsic.h
 *
 * List of intrinsic functions.
 */

#ifndef _INTRINSIC_H
#define _INTRINSIC_H

#include <math.h>
#include "myintrinsic.c"

#ifdef CONTROL
#include "example/apl.c"
#endif

typedef void (*INTRINSIC_FUNCTION) PROTO((char **));

struct INTR
	{
	char *name;
	INTRINSIC_FUNCTION adr;
	} intr_name[]=
		{{"cclose", (INTRINSIC_FUNCTION)cclose},
	         {"cgetc", (INTRINSIC_FUNCTION)cgetc},
		 {"chclose", (INTRINSIC_FUNCTION)chclose},
		 {"chflush", (INTRINSIC_FUNCTION)chflush},
		 {"chopen", (INTRINSIC_FUNCTION)chopen},  
		 {"chwrite", (INTRINSIC_FUNCTION)chwrite},
	         {"copen", (INTRINSIC_FUNCTION)copen},
	         {"cputc", (INTRINSIC_FUNCTION)cputc},
	         {"exp", (INTRINSIC_FUNCTION)exp_},
		 {"fflush", (INTRINSIC_FUNCTION)cfflush},
	         {"fprintf", (INTRINSIC_FUNCTION)cfprintf},
	         {"fscanf", (INTRINSIC_FUNCTION)cfscanf},
		 {"printf", (INTRINSIC_FUNCTION)cprintf},
	         {"scanf", (INTRINSIC_FUNCTION)cscanf},
	         {"sin", (INTRINSIC_FUNCTION)sin_}
#ifdef CONTROL
		 ,
	         {"green_func", (INTRINSIC_FUNCTION)green_func},
	         {"csigpause", (INTRINSIC_FUNCTION)csigpause},
	         {"open_termo", (INTRINSIC_FUNCTION)open_termo},
	         {"read_termo", (INTRINSIC_FUNCTION)read_termo},
	         {"write_termo", (INTRINSIC_FUNCTION)write_termo},
	         {"close_termo", (INTRINSIC_FUNCTION)close_termo},
	         {"init_wait", (INTRINSIC_FUNCTION)init_wait}
#endif
};



/* 
 * Number of intrinsic functions.
 */
#define SIZE_REM sizeof(intr_name)/sizeof(intr_name[0]) 



/* 
 * Size of an array of intrinsic functions.
 */
void (*(f[SIZE_REM])) PROTO((char **));

/* \verbatim */

#endif /* _INTRINSIC_H */
