/* 
 * buf.h
 * 
 * changes buffer size of input buffer for lexical analyzer
 *
 */

#ifndef _BUF_H
#define _BUF_H

#ifdef YYLMAX
#undef YYLMAX
#endif
#define YYLMAX 10000

#endif /* _BUF_H */
