/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#ifndef _FLAGS_H
#define _FLAGS_H

/* Flag if warnings should be */
/* printed. Default no. */
extern int warning_yes;

/* Warn if a comment beginning is in comment. */
extern int warning_comment;

/* Warn if it is something wrong in scanf, printf etc. formats. */
extern int warning_format;

/* Warn if functions or parameters are implicit defined. */
extern int warning_implicit;

/* Don't print warning messages. */
extern int warning_inhibit;

/*  Nonzero means warn about function definitions that default the return type
   or that use a null return and have a return-type other than void.  */
extern int warning_return_type;

/* If set, warn about trigraphs. */
extern int warning_trigraphs;

/* Warn about unused locals. */
extern int warning_unused;

/* Warn if a variable is used before it is initialized. */
extern int warning_uninitialized;

/* Print various extra warnings. */
extern int warning_extra;

/* Warn if a function returns an aggregate. */
extern int warning_aggregate_return;

/* Warnings are errors. */
extern int warnings_are_errors;

/* Flag in the case of errors or */
/* compile only. Default compile and */
/* execute. */
extern int no_compile_only;

/* If nonzero compile everything and than call main function. Simulate
   compiler behavior. */
extern int handle_main;

/* Nonzero means call by value should be used. Default. */
extern int call_by_value;

/* Nonzero means call by reference. */
extern int call_by_reference;

/* If nonzero, generate additional debugging information. */
extern int dbg_symbols;

#endif
