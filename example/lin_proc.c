#include <stdio.h>
#include <rpc/rpc.h>
#include "lin.h"

int *
remote_open_1(remote_op_struct)
     remote_o_struct *remote_op_struct;
{
  static int result;
  
  result = open(remote_op_struct->device,remote_op_struct->flags,remote_op_struct->mode); 
  return(&result);
}

int *
remote_close_1(fd)
     int *fd;
{
  static int result;
  

  result = close(*fd);
  return(&result);
}

remote_r_w_struct *
remote_read_1(par)
     remote_r_w_struct *par;
     
{
  static remote_r_w_struct result;
  int i;
  
  result.fd = par->fd;
  result.bytes = par->bytes;
  if(read(result.fd,result.buf,result.bytes) == -1)
    {
      result.bytes = -1;
    }
  
  return(&result);
}

int *
remote_write_1(remote_w_struct)
     remote_r_w_struct *remote_w_struct;
{
  static int result;
  
  result = write(remote_w_struct->fd,remote_w_struct->buf,remote_w_struct->bytes);
      
  return(&result);
}

