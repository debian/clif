/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include "lin.h"

bool_t
xdr_devicetype(xdrs, objp)
	register XDR *xdrs;
	devicetype *objp;
{

	register long *buf;

	if (!xdr_string(xdrs, objp, ~0))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_cbuf(xdrs, objp)
	register XDR *xdrs;
	cbuf objp;
{

	register long *buf;

	if (!xdr_vector(xdrs, (char *)objp, 256,
		sizeof (char), (xdrproc_t) xdr_char))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_remote_o_struct(xdrs, objp)
	register XDR *xdrs;
	remote_o_struct *objp;
{

	register long *buf;

	if (!xdr_devicetype(xdrs, &objp->device))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->flags))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->mode))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_remote_r_w_struct(xdrs, objp)
	register XDR *xdrs;
	remote_r_w_struct *objp;
{

	register long *buf;

	if (!xdr_int(xdrs, &objp->fd))
		return (FALSE);
	if (!xdr_cbuf(xdrs, objp->buf))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->bytes))
		return (FALSE);
	return (TRUE);
}
